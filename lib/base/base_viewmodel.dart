import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';

import '../generated/l10n.dart';
import '../main.dart';
import '../services/remote/api_client.dart';
import 'di/locator.dart';

abstract class BaseViewModel extends ChangeNotifier {
  final api = locator<ApiClient>();

  bool _isFirst = true;
  bool _showLoading = false;
  bool isLoading = false;
  late VoidCallback onShowLoading, onHideLoading;
  late void Function(String) onShowError;
  late void Function(String) onShowSuccess;

  BaseViewModel() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      onInit();
    });
  }

  @protected
  void onInit();

  void showLoading() {
    if (!_showLoading) {
      _showLoading = true;
      onShowLoading.call();
    }
  }

  void hideLoading() {
    if (_showLoading) {
      _showLoading = false;
      onHideLoading.call();
    }
  }

  void showError(String message) {
    hideLoading();
    onShowError.call(message);
  }

  void showSuccess(String message) {
    hideLoading();
    onShowSuccess.call(message);
  }

  void showErrorConnection() {
    Connectivity().checkConnectivity().then((value) {
      if (value == ConnectivityResult.none) {
        showError(S.of(MyApp.context!).message_error_lost_connect);
      }
    });
  }

  void appear() {
    if (_isFirst) {
      _isFirst = false;
      return;
    }

    try {
      onAppear.call();
    } catch (_) {}
  }

  void onAppear();

  void disAppear() {
    if (_isFirst) return;

    try {
      onDisAppear();
    } catch (_) {}
  }

  void onDisAppear();
}
