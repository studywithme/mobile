import 'package:get_it/get_it.dart';
import 'package:studywithme/models/stream/chat_stream.dart';
import 'package:studywithme/services/local/shared_prefs.dart';

import '../../models/stream/room_stream.dart';
import '../../services/remote/api_client.dart';
import '../../services/remote/socket_client.dart';

GetIt locator = GetIt.instance;
void setupLocator() {
  locator.registerLazySingleton(() => ApiClient());
  locator.registerLazySingleton(() => StreamSocket());
  locator.registerLazySingleton(() => MessageStream());
  locator.registerLazySingleton(() => RoomStream());
  SharedPrefs.init();
}
