import 'package:flutter/material.dart';
import 'package:studywithme/models/badge_type.dart';

class BadgeWidget extends StatelessWidget {
  final String image;
  final BadgeType type;
  const BadgeWidget({Key? key, required this.image, required this.type})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      width: 30,
      decoration:
          const BoxDecoration(color: Colors.yellow, shape: BoxShape.circle),
      child: Center(
        child: Container(
          height: 20,
          width: 20,
          decoration: const BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(
                      ("https://upload.wikimedia.org/wikipedia/commons/3/36/Hopetoun_falls.jpg")))),
        ),
      ),
    );
  }
}
