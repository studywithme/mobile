import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'package:provider/provider.dart';
import 'package:studywithme/pages/main/chats/chat_vm.dart';
import 'package:studywithme/pages/main/chats/group_chat/group_chat_vm.dart';
import 'package:studywithme/pages/main/room/group_room/group_room_vm.dart';

import '../../../../../generated/l10n.dart';
import '../constants/app_constants.dart';
import '../pages/main/chats/room_chat/room_chat_vm.dart';

class ChatInputField<T> extends StatelessWidget {
  final TextEditingController messageController;
  final String message;
  final Function(String) onChanged;
  final Function()? callBack;
  final Function(File file)? onSendImage;
  const ChatInputField(
      {Key? key,
      required this.messageController,
      required this.message,
      required this.onChanged,
        this.onSendImage,
      this.callBack})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: SafeArea(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Expanded(
                child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.deepPurpleAccent.withOpacity(0.1)),
              child: Row(
                children: [
                  InkWell(
                    onTap: () async{
                      XFile? file = await ImagePicker().pickImage(
                          source: ImageSource.gallery,
                          maxHeight: 1800,
                          maxWidth: 1800);
                      if (file != null) {
                        File selectedImage = File(file.path);
                        if (onSendImage != null) {
                          onSendImage!.call(selectedImage);
                        }
                      }
                    },
                    child: Icon(
                      Icons.image,
                      color: Theme.of(context)
                          .textTheme
                          .bodyText1
                          ?.color
                          ?.withOpacity(0.4),
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    child: TextField(
                      controller: messageController,
                      decoration: const InputDecoration(
                        hintText: 'Input your message',
                        border: InputBorder.none,
                      ),
                      onChanged: onChanged,
                    ),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  // InkWell(
                  //   onTap: () {
                  //     provider.attachFiles(FileType.image);
                  //   },
                  //   child: Icon(
                  //     Icons.photo_camera,
                  //     color: Theme.of(context)
                  //         .textTheme
                  //         .bodyText1
                  //         ?.color
                  //         ?.withOpacity(0.4),
                  //   ),
                  // ),
                ],
              ),
            )),
            InkWell(
              onTap: () {
                if (messageController.text.isEmpty) {
                  // TODO Send voice chat

                } else {
                  // Send text message
                  if (T is RoomChatVM) {
                    debugPrint('logo1');
                    final provider = Provider.of<RoomChatVM>(context);
                    provider.sendMessage(messageController.text);
                    provider.scrollController.animateTo(
                        provider.scrollController.position.maxScrollExtent,
                        duration: const Duration(milliseconds: 200),
                        curve: Curves.easeInOut);
                    FocusManager.instance.primaryFocus?.unfocus();
                    provider.emptyMessage();
                  } else {
                    if (callBack != null) {
                      callBack?.call();
                    }
                  }
                  if (T is ChatVM) {
                    debugPrint('log2');
                    final provider = Provider.of<ChatVM>(context);
                    provider.sendMessage(messageController.text);
                    provider.scrollController.animateTo(
                        provider.scrollController.position.maxScrollExtent,
                        duration: const Duration(milliseconds: 200),
                        curve: Curves.easeInOut);
                    FocusManager.instance.primaryFocus?.unfocus();
                    provider.emptyMessage();
                  }
                  if (T is GroupChatVM) {
                    debugPrint('logo3');
                    final provider = Provider.of<GroupChatVM>(context);
                    provider.sendMessage(messageController.text);
                    provider.scrollController.animateTo(
                        provider.scrollController.position.maxScrollExtent,
                        duration: const Duration(milliseconds: 200),
                        curve: Curves.easeInOut);
                    FocusManager.instance.primaryFocus?.unfocus();
                    provider.emptyMessage();
                  }
                }
              },
              child: Container(
                margin: const EdgeInsets.only(left: 5),
                padding: const EdgeInsets.all(10),
                decoration: const BoxDecoration(boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 6.0,
                  ),
                ], shape: BoxShape.circle, color: Colors.deepPurpleAccent),
                child: const Center(
                  child: Icon(
                    Icons.send,
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
