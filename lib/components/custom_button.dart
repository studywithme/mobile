import 'package:flutter/material.dart';

@immutable
class CustomButton extends StatelessWidget {
  final String title;
  final double radius;
  final Color color;
  final Color textColor;
  final Function onPress;
  Widget? prefixIcon;
  double? width;

  CustomButton(
      {Key? key,
      required this.title,
      required this.onPress,
      this.radius = 40,
      this.color = Colors.deepPurpleAccent,
      this.textColor = Colors.white,
      this.width,
      this.prefixIcon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        onPress.call();
      },
      style: ElevatedButton.styleFrom(
          primary: color,
          fixedSize: Size(width ?? MediaQuery.of(context).size.width, 50),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(radius))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (prefixIcon != null)
            Container(
              margin: const EdgeInsets.only(right: 10),
              child: Center(child: prefixIcon!),
            ),
          Text(
            title,
            style: TextStyle(color: textColor),
          )
        ],
      ),
    );
  }
}
