import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';

class CustomDropdown extends StatelessWidget {
  final List<String> items;
  Function(String?)? onChanged;
  Function(String?)? onSaved;
  String placeHolder;
  Color? backgroundColor;
  final String value;

  CustomDropdown(
      {Key? key,
      required this.items,
      this.placeHolder = '',
      this.backgroundColor = Colors.deepPurpleAccent,
      required this.value,
      this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField2(
      decoration: InputDecoration(
        fillColor: backgroundColor,
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
      ),
      isExpanded: true,
      hint: Text(
        placeHolder,
        style: const TextStyle(fontSize: 16),
      ),
      icon: const Icon(
        Icons.arrow_drop_down,
        color: Colors.black45,
      ),
      iconSize: 30,
      buttonHeight: 60,
      buttonPadding: const EdgeInsets.only(left: 12, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
      ),
      value: value,
      items: items
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                    fontSize: 16,
                  ),
                ),
              ))
          .toList(),
      onChanged: (String? value) {
        if (onChanged != null) {
          onChanged?.call(value);
        }
      },
      onSaved: (value) {
        //selectedValue = value.toString();
      },
    );
  }
}
