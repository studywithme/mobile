import 'package:flutter/material.dart';
import 'package:studywithme/models/goal.dart';

class CustomGoal extends StatefulWidget {
  final Goal goal;
  final Function(dynamic)? onChanged;
  final Function()? onRemoved;
  final Function(Goal)? onEdit;
  final bool isOnRoom;
  const CustomGoal(
      {Key? key,
      required this.goal,
      this.onChanged,
      this.onRemoved,
      this.isOnRoom = false,
      this.onEdit})
      : super(key: key);

  @override
  _CustomGoalState createState() => _CustomGoalState();
}

class _CustomGoalState extends State<CustomGoal> {
  late TextEditingController textController;
  final FocusNode focusNode = FocusNode();
  bool isEdit = false;
  String isComplete = 'open';
  @override
  void initState() {
    isComplete = widget.goal.status ?? 'open';
    debugPrint("Dta: ${widget.goal.toJson()}, $isComplete");
    textController = TextEditingController(text: widget.goal.title);
    super.initState();
  }

  @override
  void dispose() {
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(15)),
      child: Row(
        children: [
          Radio(
              fillColor:
                  MaterialStateColor.resolveWith((states) => Colors.green),
              activeColor: Colors.greenAccent,
              value: widget.goal.status,
              groupValue: 'completed',
              onChanged: widget.onChanged),
          isEdit
              ? SizedBox(
                  width: MediaQuery.of(context).size.width * 0.7,
                  height: 50,
                  child: TextField(
                    focusNode: focusNode,
                    controller: textController,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        suffixIcon: InkWell(
                            onTap: () {
                              setState(() {
                                isEdit = false;
                              });
                              var goal = widget.goal;
                              if (goal.title == textController.text) {
                                return;
                              }
                              goal.title = textController.text;
                              if (widget.onEdit != null) {
                                widget.onEdit?.call(goal);
                              }
                            },
                            child: const Icon(Icons.check))),
                  ),
                )
              : Text(widget.goal.title,
                  maxLines: 5,
                  style: TextStyle(
                      color: Colors.black,
                      decoration: widget.goal.status == 'completed'
                          ? TextDecoration.lineThrough
                          : TextDecoration.none)),
          const Spacer(),
          Visibility(
            visible: !isEdit && !widget.isOnRoom,
            child: InkWell(
              onTap: () {
                setState(() {
                  isEdit = true;
                });
                //focusNode.requestFocus();
              },
              child: const Icon(
                Icons.edit,
                size: 16,
              ),
            ),
          ),
          const SizedBox(
            width: 20,
          ),
        ],
      ),
    );
  }
}
