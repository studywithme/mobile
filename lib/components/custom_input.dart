import 'package:flutter/material.dart';

import '../utils/colors.dart';

class CustomInput extends StatefulWidget {
  final TextEditingController controller;
  final bool hasBorder;
  bool readonly;
  Function(String)? onChanged;
  Function()? onTap;
  TextInputType inputType;
  String? placeHolder;
  Widget? prefixIcon;
  Widget? suffixIcon;
  Color? backgroundColor;
  Color borderColor;
  bool obscureText;
  TextStyle? hintStyle;
  FocusNode? focusNode;

  CustomInput(
      {Key? key,
      required this.controller,
      this.onChanged,
      this.prefixIcon,
      this.suffixIcon,
      this.placeHolder,
      this.backgroundColor = AppColor.hF7ECFF,
      this.hasBorder = true,
      this.readonly = false,
      this.obscureText = false,
      this.onTap,
      this.hintStyle,
      this.focusNode,
      this.borderColor = const Color.fromRGBO(229, 229, 229, 1),
      this.inputType = TextInputType.text})
      : super(key: key);

  @override
  _CustomInputState createState() => _CustomInputState();
}

class _CustomInputState extends State<CustomInput> {
  late FocusNode focusNode;

  @override
  void initState() {
    focusNode = FocusNode();
    focusNode.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      focusNode: widget.focusNode,
      controller: widget.controller,
      onChanged: widget.onChanged,
      onTap: widget.onTap,
      keyboardType: widget.inputType,
      readOnly: widget.readonly,
      obscureText: widget.obscureText,
      decoration: InputDecoration(
          hintStyle: widget.hintStyle,
          hintText: widget.placeHolder,
          fillColor: widget.backgroundColor,
          filled: true,
          prefixIcon: widget.prefixIcon,
          suffixIcon: widget.suffixIcon,
          border: widget.hasBorder
              ? OutlineInputBorder(
                  borderSide: BorderSide(color: widget.borderColor, width: 0.1),
                  borderRadius: const BorderRadius.all(Radius.circular(20)))
              : InputBorder.none,
          focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              borderSide: BorderSide(color: Colors.deepPurpleAccent))),
    );
  }
}
