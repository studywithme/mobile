import 'package:flutter/material.dart';

class CustomNumberSelected extends StatelessWidget {
  final int value;
  final int minValue;
  final int maxValue;
  final Function() onMinus;
  final Function() onAdd;
  const CustomNumberSelected(
      {Key? key,
      required this.value,
      required this.onAdd,
      required this.onMinus,
      this.minValue = 2,
      this.maxValue = 10})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      decoration: BoxDecoration(
          border: Border.all(color: Colors.deepPurpleAccent.withOpacity(0.6))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InkWell(
            onTap: onMinus,
            child: Container(
              height: 40,
              width: 50,
              color: Colors.deepPurpleAccent
                  .withOpacity(value == minValue ? 0.2 : 0.8),
              child: const Center(
                child: Icon(
                  Icons.remove,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Expanded(
              child: Center(
            child: Text('$value'),
          )),
          InkWell(
            onTap: onAdd,
            child: Container(
              height: 40,
              width: 50,
              color: Colors.deepPurpleAccent
                  .withOpacity(value == maxValue ? 0.2 : 0.8),
              child: const Center(
                child: Icon(Icons.add, color: Colors.white),
              ),
            ),
          )
        ],
      ),
    );
  }
}
