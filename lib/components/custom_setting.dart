import 'package:flutter/material.dart';

class CustomSetting extends StatelessWidget {
  final Function() onTap;
  final Widget icon;
  final String label;
  bool isSwitch;
  bool hasNavigate;
  TextStyle? customStyle;
  double height;
  Function(bool)? onChanged;

  CustomSetting(
      {Key? key,
      required this.icon,
      required this.label,
      required this.onTap,
      this.isSwitch = false,
      this.hasNavigate = true,
      this.customStyle,
      this.height = 40,
      this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: SizedBox(
        height: height,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            icon,
            const SizedBox(
              width: 10,
            ),
            Text(
              label,
              style: customStyle,
            ),
            const Spacer(),
            if (hasNavigate)
              isSwitch
                  ? Switch(value: true, onChanged: onChanged)
                  : const Icon(
                      Icons.navigate_next,
                      color: Colors.black,
                    )
          ],
        ),
      ),
    );
  }
}
