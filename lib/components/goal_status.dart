import 'package:flutter/material.dart';
import 'package:studywithme/models/goal.dart';

class GoalStatus extends StatelessWidget {
  final List<Goal> goals;
  const GoalStatus({Key? key, required this.goals}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var finished = 0;
    for (var element in goals) { if (element.status == 'completed') {finished++;}}
    return Container(
      height: 50,
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: BoxDecoration(
          color: Colors.black.withOpacity(0.8),
          borderRadius: BorderRadius.circular(15)),
      child: Column(
        children: [
          Row(
            children: const [
              Icon(Icons.flag_outlined, color: Colors.white),
              SizedBox(
                width: 5,
              ),
              Text('Session Goals', style: TextStyle(color: Colors.white))
            ],
          ),
          Text('$finished/${goals.length}', style: const TextStyle(color: Colors.white))
        ],
      ),
    );
  }
}
