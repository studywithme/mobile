import 'dart:io';

import 'package:flutter/material.dart';

import '../models/message.dart';

class ImageMessage extends StatelessWidget {
  final Message message;

  const ImageMessage({Key? key, required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 200),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          message.isFirst ?? false
              ? Image.file(File(message.message))
              : Image.network(message.message)
        ],
      ),
    );
  }
}
