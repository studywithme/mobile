import 'package:flutter/material.dart';
import 'package:studywithme/components/text_message.dart';

import '../models/message.dart';
import 'image_message.dart';

class MessageItem extends StatelessWidget {
  final Message message;

  const MessageItem({Key? key, required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Row(
        mainAxisAlignment:
            message.isSender ? MainAxisAlignment.end : MainAxisAlignment.start,
        children: [getMessageContent()],
      ),
    );
  }

  Widget getMessageContent() {
    //return TextMessage(message: message);
    switch (message.messageType) {
      case 'text':
        return TextMessage(message: message);
      case 'image':
        return ImageMessage(
          message: message,
        );
      default:
        return const SizedBox();
    }
  }
}
