import 'package:flutter/material.dart';

class RoomButton extends StatelessWidget {
  final Widget icon;
  final Function()? onTap;
  const RoomButton({Key? key, required this.icon, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: BoxDecoration(
            color: Colors.black.withOpacity(0.8),
            borderRadius: BorderRadius.circular(15)),
        child: Center(
          child: icon,
        ),
      ),
    );
  }
}
