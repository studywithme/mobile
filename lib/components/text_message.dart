import 'package:flutter/material.dart';
import '../../../../../utils/converter.dart';
import '../../../../../models/message.dart';

class TextMessage extends StatelessWidget {
  final Message message;

  const TextMessage({Key? key, required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.all(15),
          decoration: BoxDecoration(
              color: message.isSender
                  ? Colors.deepPurpleAccent
                  : Colors.deepPurpleAccent.withOpacity(0.09),
              borderRadius: BorderRadius.only(
                  topRight: const Radius.circular(15),
                  topLeft: Radius.circular(!message.isSender ? 5 : 15),
                  bottomRight: Radius.circular(message.isSender ? 5 : 15),
                  bottomLeft: const Radius.circular(15))),
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 200),
            child: Padding(
              padding: const EdgeInsets.only(right: 35, bottom: 0),
              child: Row(
                children: [
                  message.isSender
                      ? Container()
                      : Container(
                          height: 20,
                          width: 20,
                          decoration: const BoxDecoration(
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(
                                      'https://banner2.cleanpng.com/20180402/ojw/kisspng-united-states-avatar-organization-information-user-avatar-5ac20804a62b58.8673620215226654766806.jpg'))),
                        ),
                  Text(
                    message.message,
                    style: TextStyle(
                        color: message.isSender ? Colors.white : Colors.black),
                  )
                ],
              ),
            ),
          ),
        ),
        Positioned(
            bottom: 10,
            right: 5,
            child: Opacity(
              opacity: 0.4,
              child: Text(message.time?.dateTime ?? '',
                  style: TextStyle(
                      color: message.isSender ? Colors.white : Colors.black)),
            ))
      ],
    );
  }
}
