import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/countdown_timer_controller.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';

class TimeCounter extends StatefulWidget {
  final bool isOnBreak;
  final bool isPersonal;
  final CountdownTimerController controller;
  const TimeCounter(
      {Key? key,
      required this.controller,
      required this.isOnBreak,
      this.isPersonal = true})
      : super(key: key);

  @override
  _TimeCounterState createState() => _TimeCounterState();
}

class _TimeCounterState extends State<TimeCounter> {
  late CountdownTimerController controller;
  late int endTime;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    controller = widget.controller;
    return Container(
      height: 50,
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: BoxDecoration(
          color: Colors.black.withOpacity(0.8),
          borderRadius: BorderRadius.circular(15)),
      child: Column(
        children: [
          Row(
            children: [
              const Icon(
                Icons.menu_book,
                color: Colors.white,
              ),
              const SizedBox(
                width: 5,
              ),
              Text(
                widget.isOnBreak
                    ? 'Break Time'
                    : widget.isPersonal
                        ? 'Personal Timer'
                        : 'Group Timer',
                style: const TextStyle(color: Colors.white),
              )
            ],
          ),
          CountdownTimer(
            controller: controller,
            textStyle: const TextStyle(color: Colors.white),
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    controller.dispose();
    super.dispose();
  }
}
