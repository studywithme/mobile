class SocketEvent {
  // ROOM
  static const joinRoom = 'room:join';
  static const leftRoom = 'room:left';

  static const joinSingleRoom = 'room_single:join';
  static const leftSingleRoom = 'room_single:left';

  static const breakRoom = 'room:break';
  static const overBreakRoom = 'room:over_break';

  static const userChangeSetting = 'room:user_change_setting';
  static const changeSetting = 'room:change_setting';

  static const joinRoomSuccess = 'room:join_success';
  static const joinRoomFail = 'room:join_fail';
  static const userJoined = 'room:user_joined';
  static const userLeft = 'room:user_left';

  static const newRoom = 'room:new_room_created';

  // CHAT
  static const joinChannel = 'chat:join_room';
  static const leftChannel = 'chat:left_room';
  static const sendMessage = 'chat:send_message';

  static const userSentMessage = 'chat:user_send_message';
}
