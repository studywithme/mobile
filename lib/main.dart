// import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:studywithme/base/di/locator.dart';
import 'package:studywithme/pages/splash/splash_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  // await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static GlobalKey<NavigatorState> navigateKey = GlobalKey();
  static BuildContext? context = navigateKey.currentContext;
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Study with me',
      theme: ThemeData(
          primarySwatch: Colors.deepPurple,
          unselectedWidgetColor: Colors.deepPurpleAccent),
      darkTheme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: Colors.blue[300],
        brightness: Brightness.dark,
        backgroundColor: Colors.grey[900],
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: const SplashPage(),
    );
  }
}
