import 'package:json_annotation/json_annotation.dart';

import 'board.dart';
part 'all_rank.g.dart';

@JsonSerializable()
class AllRank {
  late List<Board> board;

  AllRank();

  factory AllRank.fromJson(Map<String, dynamic> json) =>
      _$AllRankFromJson(json);
}
