// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'all_rank.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AllRank _$AllRankFromJson(Map<String, dynamic> json) => AllRank()
  ..board = (json['board'] as List<dynamic>)
      .map((e) => Board.fromJson(e as Map<String, dynamic>))
      .toList();

Map<String, dynamic> _$AllRankToJson(AllRank instance) => <String, dynamic>{
      'board': instance.board,
    };
