import 'package:json_annotation/json_annotation.dart';
import 'package:studywithme/models/user_model.dart';
part 'board.g.dart';

@JsonSerializable()
class Board {
  double? hour;
  late int rank;
  late UserModel user;

  Board();

  factory Board.fromJson(Map<String, dynamic> json) => _$BoardFromJson(json);
}
