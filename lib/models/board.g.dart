// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'board.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Board _$BoardFromJson(Map<String, dynamic> json) => Board()
  ..hour = (json['hour'] as num?)?.toDouble()
  ..rank = json['rank'] as int
  ..user = UserModel.fromJson(json['user'] as Map<String, dynamic>);

Map<String, dynamic> _$BoardToJson(Board instance) => <String, dynamic>{
      'hour': instance.hour,
      'rank': instance.rank,
      'user': instance.user,
    };
