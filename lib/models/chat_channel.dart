import 'package:json_annotation/json_annotation.dart';
import 'message.dart';
part 'chat_channel.g.dart';

@JsonSerializable()
class ChatChannel {
  late int id;
  late String name;
  late String type;
  bool? isDeleted;

  List<Message>? messages;

  ChatChannel();

  factory ChatChannel.fromJson(Map<String, dynamic> json) =>
      _$ChatChannelFromJson(json);
}
