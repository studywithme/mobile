// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_channel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatChannel _$ChatChannelFromJson(Map<String, dynamic> json) => ChatChannel()
  ..id = json['id'] as int
  ..name = json['name'] as String
  ..type = json['type'] as String
  ..isDeleted = json['isDeleted'] as bool?
  ..messages = (json['messages'] as List<dynamic>?)
      ?.map((e) => Message.fromJson(e as Map<String, dynamic>))
      .toList();

Map<String, dynamic> _$ChatChannelToJson(ChatChannel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'type': instance.type,
      'isDeleted': instance.isDeleted,
      'messages': instance.messages,
    };
