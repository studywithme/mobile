import 'package:json_annotation/json_annotation.dart';

import 'board.dart';
part 'daily.g.dart';

@JsonSerializable()
class Daily {
  late List<Board> board;

  Daily();

  factory Daily.fromJson(Map<String, dynamic> json) => _$DailyFromJson(json);
}
