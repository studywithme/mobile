// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'daily.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Daily _$DailyFromJson(Map<String, dynamic> json) => Daily()
  ..board = (json['board'] as List<dynamic>)
      .map((e) => Board.fromJson(e as Map<String, dynamic>))
      .toList();

Map<String, dynamic> _$DailyToJson(Daily instance) => <String, dynamic>{
      'board': instance.board,
    };
