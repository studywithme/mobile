import 'package:json_annotation/json_annotation.dart';
part 'goal.g.dart';

@JsonSerializable()
class Goal {
  int? id;

  late String title;

  String? status;

  @JsonKey(ignore: true)
  bool onEdit = false;

  Goal();

  factory Goal.fromJson(Map<String, dynamic> json) => _$GoalFromJson(json);

  Map<String, dynamic> toJson() => _$GoalToJson(this);
}
