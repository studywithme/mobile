import 'package:json_annotation/json_annotation.dart';
import 'package:studywithme/models/user_model.dart';

part 'message.g.dart';

enum MessageType { text, image }

@JsonSerializable()
class Message {
  int? id;

  late String message;

  //List<String>? images;

  String? time;

  String? messageType;
  UserModel? user;

  late bool isSender;

  bool? isFirst;

  Message();

  factory Message.fromJson(Map<String, dynamic> json) =>
      _$MessageFromJson(json);

}
