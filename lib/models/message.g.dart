// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Message _$MessageFromJson(Map<String, dynamic> json) => Message()
  ..id = json['id'] as int?
  ..message = json['message'] as String
  ..time = json['time'] as String?
  ..user = json['user'] == null
      ? null
      : UserModel.fromJson(json['user'] as Map<String, dynamic>)
  ..messageType = json['type'] as String?
  ..isSender = json['isSender'] as bool
  ..isFirst = json['isFirst'] as bool?;

Map<String, dynamic> _$MessageToJson(Message instance) => <String, dynamic>{
      'message': instance.message,
      'time': instance.time,
      'user': instance.user,
      'isSender': instance.isSender,
    };
