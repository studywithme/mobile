import 'package:json_annotation/json_annotation.dart';

import 'board.dart';
part 'monthly.g.dart';

@JsonSerializable()
class Monthly {
  late List<Board> board;

  Monthly();

  factory Monthly.fromJson(Map<String, dynamic> json) =>
      _$MonthlyFromJson(json);
}
