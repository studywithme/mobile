// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'monthly.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Monthly _$MonthlyFromJson(Map<String, dynamic> json) => Monthly()
  ..board = (json['board'] as List<dynamic>)
      .map((e) => Board.fromJson(e as Map<String, dynamic>))
      .toList();

Map<String, dynamic> _$MonthlyToJson(Monthly instance) => <String, dynamic>{
      'board': instance.board,
    };
