import 'package:json_annotation/json_annotation.dart';
import 'package:studywithme/models/study_detail.dart';
part 'room.g.dart';

@JsonSerializable()
class Room {
  @JsonKey(name: 'id')
  late int id;

  @JsonKey(name: 'name')
  late String name;

  @JsonKey(name: 'description')
  String? description;

  @JsonKey(name: 'maxCount')
  late int maxCount;

  @JsonKey(name: 'isFavourite')
  late bool isFavourite;

  @JsonKey(name: 'author')
  String? author;

  @JsonKey(name: 'isAuthor')
  bool? isAuthor;

  @JsonKey(name: 'background')
  String? background;

  @JsonKey(name: 'status')
  late RoomStatus status;

  @JsonKey(name: 'type')
  late RoomType type;

  @JsonKey(name: 'currentStudentNumber')
  int? currentUsers = 1;

  @JsonKey(name: 'listUser')
  List<StudyDetail>? listUser;

  @JsonKey(name: 'lastedJoinTime')
  String? lastedJoinTime;

  Room();

  factory Room.fromJson(Map<String, dynamic> json) => _$RoomFromJson(json);

  Map<String, dynamic> toJson() => _$RoomToJson(this);
}

enum RoomStatus { live, finished }

enum RoomType { solo, group }
