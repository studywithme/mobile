// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Room _$RoomFromJson(Map<String, dynamic> json) => Room()
  ..id = json['id'] as int
  ..name = json['name'] as String
  ..description = json['description'] as String?
  ..maxCount = json['maxCount'] as int
  ..isFavourite = json['isFavourite'] as bool
  ..author = json['author'] as String?
  ..isAuthor = json['isAuthor'] as bool?
  ..background = json['background'] as String?
  ..lastedJoinTime = json['lastedJoinTime'] as String?
  ..status = $enumDecode(_$RoomStatusEnumMap, json['status'])
  ..type = $enumDecode(_$RoomTypeEnumMap, json['type'])
  ..currentUsers = json['currentStudentNumber'] as int?
  ..listUser = (json['listUser'] as List<dynamic>?)
      ?.map((e) => StudyDetail.fromJson(e as Map<String, dynamic>))
      .toList();

Map<String, dynamic> _$RoomToJson(Room instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
      'maxCount': instance.maxCount,
      'isFavourite': instance.isFavourite,
      'author': instance.author,
      'isAuthor': instance.isAuthor,
      'background': instance.background,
      'status': _$RoomStatusEnumMap[instance.status]!,
      'type': _$RoomTypeEnumMap[instance.type]!,
      'currentStudentNumber': instance.currentUsers,
      'listUser': instance.listUser,
    };

const _$RoomStatusEnumMap = {
  RoomStatus.live: 'live',
  RoomStatus.finished: 'finished',
};

const _$RoomTypeEnumMap = {
  RoomType.solo: 'solo',
  RoomType.group: 'group',
};
