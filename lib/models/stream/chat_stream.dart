import 'dart:async';

import 'package:studywithme/models/message.dart';
import 'package:studywithme/models/study_detail.dart';

class MessageStream {
  int counter = 0;
  StreamController messageController =
      StreamController<List<Message>>.broadcast();
  Stream get messageStream => messageController.stream;

  void dispose() {
    messageController.close();
  }
}
