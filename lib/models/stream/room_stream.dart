import 'dart:async';

import 'package:studywithme/models/study_detail.dart';

class RoomStream {
  StreamController studyController =
      StreamController<List<StudyDetail>>.broadcast();
  Stream get studyStream => studyController.stream;

  void dispose() {
    studyController.close();
  }
}
