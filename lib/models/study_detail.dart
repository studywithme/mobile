import 'package:json_annotation/json_annotation.dart';
import 'package:studywithme/models/study_session.dart';
part 'study_detail.g.dart';

@JsonSerializable()
class StudyDetail {
  @JsonKey(name: 'id')
  late int id;

  @JsonKey(name: 'email')
  late String email;

  @JsonKey(name: 'name')
  String? name;

  @JsonKey(name: 'avatar')
  String? avatar;

  @JsonKey(name: 'studySession')
  late StudySession studySession;

  StudyDetail();

  factory StudyDetail.fromJson(Map<String, dynamic> json) =>
      _$StudyDetailFromJson(json);

  Map<String, dynamic> toJson() => _$StudyDetailToJson(this);
}
