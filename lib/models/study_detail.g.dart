// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'study_detail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StudyDetail _$StudyDetailFromJson(Map<String, dynamic> json) => StudyDetail()
  ..id = json['id'] as int
  ..email = json['email'] as String
  ..name = json['name'] as String?
  ..avatar = json['avatar'] as String?
  ..studySession =
      StudySession.fromJson(json['studySession'] as Map<String, dynamic>);

Map<String, dynamic> _$StudyDetailToJson(StudyDetail instance) =>
    <String, dynamic>{
      'id': instance.id,
      'email': instance.email,
      'name': instance.name,
      'avatar': instance.avatar,
      'studySession': instance.studySession,
    };
