import 'package:json_annotation/json_annotation.dart';
part 'study_session.g.dart';

@JsonSerializable()
class StudySession {
  @JsonKey(name: 'isAlive')
  late bool isAlive;

  @JsonKey(name: 'joinTime')
  late String joinTime;

  @JsonKey(name: 'leftTime')
  String? leftTime;

  @JsonKey(name: 'isCamera')
  bool? isCamera;

  @JsonKey(name: 'isMicro')
  bool? isMicro;

  StudySession();

  factory StudySession.fromJson(Map<String, dynamic> json) =>
      _$StudySessionFromJson(json);

  Map<String, dynamic> toJson() => _$StudySessionToJson(this);
}
