// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'study_session.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StudySession _$StudySessionFromJson(Map<String, dynamic> json) => StudySession()
  ..isAlive = json['isAlive'] as bool
  ..joinTime = json['joinTime'] as String
  ..leftTime = json['leftTime'] as String?
  ..isCamera = json['isCamera'] as bool?
  ..isMicro = json['isMicro'] as bool?;

Map<String, dynamic> _$StudySessionToJson(StudySession instance) =>
    <String, dynamic>{
      'isAlive': instance.isAlive,
      'joinTime': instance.joinTime,
      'leftTime': instance.leftTime,
      'isCamera': instance.isCamera,
      'isMicro': instance.isMicro,
    };
