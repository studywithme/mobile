import 'dart:core';

import 'package:flutter/material.dart';

class TabModel {
  late String label;

  late Widget icon;

  late Widget tabContent;

  TabModel({required this.label, required this.icon, required this.tabContent});
}
