import 'dart:io';

import 'package:json_annotation/json_annotation.dart';
part 'user_model.g.dart';

@JsonSerializable()
class UserModel {
  @JsonKey(name: 'id')
  int? uuid;

  @JsonKey(name: 'email')
  late String email = '';

  @JsonKey(name: 'username')
  String? username = '';

  @JsonKey(name: 'name')
  String? name = '';

  @JsonKey(name: 'password')
  String? password = '';

  @JsonKey(name: 'deviceToken')
  String? deviceToken = '';

  @JsonKey(name: 'codePhone')
  String? codePhone = '+84';

  @JsonKey(name: 'phone')
  String? phoneNumber = '';

  @JsonKey(name: 'avatar')
  String? avatar = '';

  @JsonKey(name: 'gender')
  int? gender = 0;

  @JsonKey(name: 'role')
  int? role = 2;

  UserModel();

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}
