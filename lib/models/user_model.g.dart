// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) => UserModel()
  ..uuid = json['id'] as int?
  ..email = json['email'] as String
  ..username = json['username'] as String?
  ..name = json['name'] as String?
  ..password = json['password'] as String?
  ..deviceToken = json['deviceToken'] as String?
  ..codePhone = json['codePhone'] as String?
  ..phoneNumber = json['phone'] as String?
  ..avatar = json['avatar'] as String?
  ..gender = json['gender'] as int?
  ..role = json['role'] as int?;

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'id': instance.uuid,
      'email': instance.email,
      'username': instance.username,
      'name': instance.name,
      'password': instance.password,
      'deviceToken': instance.deviceToken,
      'codePhone': instance.codePhone,
      'phone': instance.phoneNumber,
      'avatar': instance.avatar,
      'gender': instance.gender,
      'role': instance.role,
    };
