import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:studywithme/base/di/locator.dart';
import 'package:studywithme/constants/app_constants.dart';

import 'package:studywithme/services/local/shared_prefs.dart';
import 'package:studywithme/services/remote/bodies/login_body.dart';

import 'package:http/http.dart' as http;

import '../../base/base_viewmodel.dart';
import '../../models/user_model.dart';
import '../../services/remote/api_client.dart';

class AuthVM extends BaseViewModel {
  final user = UserModel();
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  late bool remember = false;
  bool isLogin = false;
  bool showPassword = false;

  @override
  void onInit() {
    var loginData = SharedPrefs.getValue('loginData');
    if (loginData.isNotEmpty) {
      var data = jsonDecode(loginData);
      remember = true;
      usernameController.text = data['email'];
      passwordController.text = data['password'];
      notifyListeners();
    }
  }

  void initData(Map<String, dynamic> data) {
    usernameController.text = data['email'];
    notifyListeners();
  }

  void navigatorPage(BuildContext context) async {
    // TODO call api to check account has been setting all?
    var hasInitAccount = false;
    user.email = usernameController.text;
    user.password = passwordController.text;
  }

  void visibilityPassword() {
    showPassword = !showPassword;
    notifyListeners();
  }

  Future<void> login({required Function() onSuccess}) async {
    showLoading();
    try {
      final authService = locator<ApiClient>().authService;
      final profileResponse = await authService.login(LoginBody.fromJson({
        'email': usernameController.text,
        'password': passwordController.text
      }));
      final data = profileResponse.data;
      final token = profileResponse.token;
      await SharedPrefs.setValue('userData', jsonEncode(data.toJson()));
      await SharedPrefs.setValue(AppConstant.accessToken, token ?? '');
      hideLoading();
      onSuccess.call();
    } on DioError catch (e) {
      showError(e.message);
    }
  }

  Future<void> register() async {
    showLoading();
    try {
      // String? token = await FirebaseMessaging.instance.getToken()
      String? token;
      final authService = locator<ApiClient>().authService;
      await authService.register(jsonEncode({
        'email': usernameController.text,
        'password': passwordController.text,
        'deviceToken': token ?? ''
      }));
      showSuccess(
          'Đăng kí tài khoản thành công! Vui lòng đăng nhập lại để tiếp tục!');
    } on DioError catch (e) {
      showError(e.message);
    }
  }

  @override
  void onAppear() {
    // TODO: implement onAppear
  }

  @override
  void onDisAppear() {
    // TODO: implement onDisAppear
  }
}
