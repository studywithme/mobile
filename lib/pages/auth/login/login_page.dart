import 'package:flutter/material.dart';
import 'package:studywithme/pages/auth/register/register_page.dart';
import 'package:studywithme/pages/main/app_page.dart';
import '../../../base/base_page.dart';
import '../../../components/custom_button.dart';
import '../../../components/custom_input.dart';
import '../auth_viewmodel.dart';
import '../../../utils/size_common.dart';

class LoginPage extends StatelessWidget with MixinBasePage<AuthVM> {
  LoginPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return builder(() => Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            leading: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: const Icon(
                Icons.arrow_back,
                color: Colors.black,
              ),
            ),
            backgroundColor: Colors.transparent,
            elevation: 0.0,
          ),
          body: Container(
            color: Colors.white,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: context.height * 0.35,
                    width: double.infinity,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned.fill(child: Container()),
                        Positioned(
                            bottom: 20,
                            child: SizedBox(
                              width: context.width,
                              child: const Align(
                                alignment: Alignment.center,
                                child: Text(
                                  'Login',
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ))
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomInput(
                    controller: provider.usernameController,
                    placeHolder: 'Email',
                    prefixIcon: const Icon(Icons.email_outlined),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomInput(
                    controller: provider.passwordController,
                    placeHolder: 'Password',
                    prefixIcon: const Icon(Icons.lock_outline),
                    obscureText: !provider.showPassword,
                    inputType: TextInputType.visiblePassword,
                    suffixIcon: InkWell(
                      onTap: provider.visibilityPassword,
                      child: Icon(provider.showPassword
                          ? Icons.visibility
                          : Icons.visibility_off),
                    ),
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  CustomButton(
                      title: 'Đăng nhập',
                      onPress: () {
                        provider.login(
                            onSuccess: () => {
                                  // navigate to home
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              const AppPage()))
                                });
                      }),
                  const SizedBox(
                    height: 25,
                  ),
                  // InkWell(
                  //   onTap: () {},
                  //   child: const Text(
                  //     'Quên mật khẩu',
                  //     style: TextStyle(color: Colors.deepPurpleAccent),
                  //   ),
                  // ),
                  // const SizedBox(
                  //   height: 20,
                  // ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'Chưa có tài khoản',
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => RegisterPage()));
                        },
                        child: const Text(
                          'Đăng ký',
                          style: TextStyle(color: Colors.deepPurpleAccent),
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  @override
  void initialise(BuildContext context) {}

  @override
  AuthVM create() {
    return AuthVM();
  }
}
