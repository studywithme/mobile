import 'package:flutter/material.dart';

import '../../base/base_page.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

import 'app_vm.dart';

class AppPage extends StatefulWidget {
  const AppPage({Key? key}) : super(key: key);

  @override
  _AppPageState createState() => _AppPageState();
}

class _AppPageState extends State<AppPage> with MixinBasePage<AppViewModel> {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return builder(() => Scaffold(
          bottomNavigationBar: Visibility(
            visible: provider.visibleBottom,
            child: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              items: provider.tabs
                  .map((e) =>
                      BottomNavigationBarItem(icon: e.icon, label: e.label))
                  .toList(),
              currentIndex: provider.currentTab,
              selectedItemColor: Colors.deepPurpleAccent,
              unselectedItemColor: Colors.blueGrey,
              showUnselectedLabels: true,
              selectedLabelStyle:
                  const TextStyle(color: Colors.deepPurpleAccent),
              onTap: (index) {
                provider.changeTab(index);
                provider.controller.animateToPage(index,
                    duration: const Duration(milliseconds: 100),
                    curve: Curves.bounceInOut);
              },
            ),
          ),
          body: PageView(
            scrollDirection: Axis.horizontal,
            controller: provider.controller,
            physics: const NeverScrollableScrollPhysics(),
            children: provider.tabs.map((e) => e.tabContent).toList(),
          ),
        ));
  }

  @override
  AppViewModel create() {
    return AppViewModel();
  }

  @override
  void initialise(BuildContext context) {
    // TODO: implement initialise
    provider.setContext(context);
  }
}
