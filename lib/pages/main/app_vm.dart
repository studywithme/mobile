import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:studywithme/base/di/locator.dart';
import 'package:studywithme/constants/app_constants.dart';
import 'package:studywithme/constants/socket_event.dart';
import 'package:studywithme/pages/main/boards/board_page.dart';
import 'package:studywithme/pages/main/chats/chat_page.dart';
import 'package:studywithme/pages/main/goals/goal_page.dart';
import 'package:studywithme/pages/main/profile/profile_page.dart';
import 'package:studywithme/pages/main/room/room_page.dart';
import 'package:studywithme/pages/main/room/room_vm.dart';
import 'package:studywithme/services/local/shared_prefs.dart';
import 'package:studywithme/services/remote/socket_client.dart';
import '../../base/base_viewmodel.dart';
import '../../models/tab_model.dart';

class AppViewModel extends BaseViewModel {
  bool isConnected = false;
  bool visibleBottom = true;
  final PageController controller = PageController();
  late BuildContext context;
  final List<TabModel> tabs = [
    TabModel(
        label: 'Room',
        icon: const Icon(Icons.videocam_rounded),
        tabContent: ChangeNotifierProvider(
            create: (context) => RoomVM(), child: const RoomPage())),
    TabModel(
        label: 'Goals',
        icon: const Icon(Icons.flag),
        tabContent: const GoalPage()),
    TabModel(
        label: 'Chats',
        icon: const Icon(Icons.chat),
        tabContent: const ChatPage()),
    TabModel(
        label: 'Boards',
        icon: const Icon(Icons.language),
        tabContent: const BoardPage()),
    TabModel(
        label: 'Profile',
        icon: const Icon(Icons.person),
        tabContent: ProfilePage()),
  ];
  late int currentTab = 0; // default home page

  @override
  void onInit() {
    initSocket();
  }

  void setContext(BuildContext context) {
    this.context = context;
  }

  void changeTab(int index) {
    currentTab = index;
    notifyListeners();
  }

  Widget getContent() {
    return tabs[currentTab].tabContent;
  }

  void visibleBottomNavigator(bool visible) {
    visibleBottom = visible;
    notifyListeners();
  }

  void gotoChatRoom() {
    currentTab = 3;
    controller.jumpToPage(currentTab);
    notifyListeners();
    Navigator.pushNamed(context, '/chat');
  }

  void initSocket() {
    var token = SharedPrefs.getValue(AppConstant.accessToken);
    IO.Socket socket = IO.io(AppConstant.socketUrl, <String, dynamic>{
      'transports': ['websocket', 'polling'],
      'autoConnect': true,
      'query': {'token': token}
    });
    debugPrint('TOKEN socket $token');
    // set up socket listener service
    locator<StreamSocket>().setupListener(socket);
  }

  @override
  void onAppear() {
    // TODO: implement onAppear
  }

  @override
  void onDisAppear() {
    // TODO: implement onDisAppear
  }
}
