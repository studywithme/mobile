import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:studywithme/base/base_page.dart';
import 'package:studywithme/gen/assets.gen.dart';
import 'package:studywithme/pages/main/boards/board_vm.dart';
import '../../../utils/converter.dart';
import '../../../utils/colors.dart';

class BoardPage extends StatefulWidget {
  const BoardPage({Key? key}) : super(key: key);

  @override
  _BoardPageState createState() => _BoardPageState();
}

class _BoardPageState extends State<BoardPage> with MixinBasePage<BoardVM> {
  @override
  Widget build(BuildContext context) {
    return builder(() => Scaffold(
          backgroundColor: AppColor.hf4f2f2,
          body: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    height: 200,
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white),
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 70,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Container(
                                height: 100,
                                decoration: BoxDecoration(
                                    color: AppColor.hf4f2f2,
                                    borderRadius: BorderRadius.circular(20)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                        height: 40,
                                        width: 40,
                                        padding: const EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: const Icon(
                                          Icons.leaderboard,
                                          color: Colors.deepPurpleAccent,
                                        )),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        const Text('Rank'),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          '#${provider.isLoading ? '0' : provider.myAll.rank}',
                                          style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 30,
                            ),
                            Expanded(
                              child: Container(
                                height: 100,
                                decoration: BoxDecoration(
                                    color: AppColor.hf4f2f2,
                                    borderRadius: BorderRadius.circular(20)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                        height: 40,
                                        width: 40,
                                        padding: const EdgeInsets.all(5),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: const Icon(
                                          Icons.timer,
                                          color: Colors.deepPurpleAccent,
                                        )),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        const Text('Study Time'),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          '${provider.isLoading ? '0' : provider.myAll.hour} hours',
                                          style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        // Row(
                        //   children: [
                        //     Expanded(
                        //       child: Container(
                        //         height: 100,
                        //         decoration: BoxDecoration(
                        //             color: AppColor.hf4f2f2,
                        //             borderRadius: BorderRadius.circular(20)),
                        //         child: Row(
                        //           mainAxisAlignment: MainAxisAlignment.center,
                        //           children: [
                        //             Container(
                        //                 height: 40,
                        //                 width: 40,
                        //                 padding: EdgeInsets.all(5),
                        //                 decoration: BoxDecoration(
                        //                     color: Colors.white,
                        //                     borderRadius:
                        //                         BorderRadius.circular(10)),
                        //                 child: Icon(
                        //                   Icons.timeline,
                        //                   color: Colors.deepPurpleAccent,
                        //                 )),
                        //             SizedBox(
                        //               width: 10,
                        //             ),
                        //             Column(
                        //               mainAxisAlignment:
                        //                   MainAxisAlignment.center,
                        //               children: [
                        //                 Text('Average/day'),
                        //                 SizedBox(
                        //                   height: 10,
                        //                 ),
                        //                 Text(
                        //                   '0 hours',
                        //                   style: TextStyle(
                        //                       fontWeight: FontWeight.bold,
                        //                       fontSize: 16),
                        //                 )
                        //               ],
                        //             )
                        //           ],
                        //         ),
                        //       ),
                        //     ),
                        //     SizedBox(
                        //       width: 30,
                        //     ),
                        //     Expanded(
                        //       child: Container(
                        //         height: 100,
                        //         decoration: BoxDecoration(
                        //             color: AppColor.hf4f2f2,
                        //             borderRadius: BorderRadius.circular(20)),
                        //         child: Row(
                        //           mainAxisAlignment: MainAxisAlignment.center,
                        //           children: [
                        //             // Container(
                        //             //     height: 40,
                        //             //     width: 40,
                        //             //     padding: EdgeInsets.all(5),
                        //             //     decoration: BoxDecoration(
                        //             //         color: Colors.white,
                        //             //         borderRadius:
                        //             //             BorderRadius.circular(10)),
                        //             //     child: Icon(
                        //             //       Icons.timer,
                        //             //       color: Colors.deepPurpleAccent,
                        //             //     )),
                        //             // SizedBox(
                        //             //   width: 10,
                        //             // ),
                        //             // Column(
                        //             //   mainAxisAlignment:
                        //             //       MainAxisAlignment.center,
                        //             //   children: [
                        //             //     Text('Study Time'),
                        //             //     SizedBox(
                        //             //       height: 10,
                        //             //     ),
                        //             //     Text(
                        //             //       '2 hours',
                        //             //       style: TextStyle(
                        //             //           fontWeight: FontWeight.bold,
                        //             //           fontSize: 16),
                        //             //     )
                        //             //   ],
                        //             // )
                        //           ],
                        //         ),
                        //       ),
                        //     ),
                        //   ],
                        // )
                      ],
                    )),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  height: MediaQuery.of(context).size.height - 250,
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: AppColor.hf4f2f2),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Text(
                        'Leader Board',
                        style: TextStyle(fontSize: 18),
                      ),
                      Container(
                        padding:
                            const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                        height: MediaQuery.of(context).size.height * 0.6 - 60,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                const Spacer(),
                                InkWell(
                                  onTap: provider.changeViewIndex,
                                  child: Text(
                                    provider.viewIndex == 0
                                        ? 'Today'
                                        : (provider.viewIndex == 1
                                            ? 'This Month'
                                            : 'All'),
                                    style: const TextStyle(
                                      color: Colors.deepPurpleAccent,
                                      decoration: TextDecoration.underline,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.6 -
                                  100,
                              child: provider.isLoading
                                  ? const Center(
                                      child: CircularProgressIndicator(),
                                    )
                                  : ListView.builder(
                                      itemCount: provider.current.length,
                                      itemBuilder: (context, index) =>
                                          SizedBox(
                                            height: 50,
                                            child: Row(
                                              children: [
                                                Text(
                                                    '${provider.current[index].rank}'),
                                                const SizedBox(
                                                  width: 30,
                                                ),
                                                Container(
                                                  height: 30,
                                                  width: 40,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      border: const Border(
                                                          bottom:  BorderSide(
                                                              color: Colors
                                                                  .black12)),
                                                      image: DecorationImage(
                                                          fit: BoxFit.cover,
                                                          image: NetworkImage(
                                                              provider
                                                                      .current[
                                                                          index]
                                                                      .user
                                                                      .avatar ??
                                                                  ""))),
                                                ),
                                                const SizedBox(
                                                  width: 40,
                                                ),
                                                SizedBox(
                                                  width: 100,
                                                  child: Text(provider
                                                          .current[index]
                                                          .user
                                                          .name ??
                                                      provider.current[index]
                                                          .user.email.username),
                                                ),
                                                const Spacer(),
                                                Text(
                                                    '${provider.current[index].hour} h')
                                              ],
                                            ),
                                          )),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }

  @override
  BoardVM create() => BoardVM();

  @override
  void initialise(BuildContext context) {
    // TODO: implement initialise
  }
}
