import 'dart:convert';

import 'package:studywithme/base/base_viewmodel.dart';
import 'package:studywithme/models/all_rank.dart';
import 'package:studywithme/models/board.dart';
import 'package:studywithme/models/daily.dart';
import 'package:studywithme/models/monthly.dart';

import '../../../base/di/locator.dart';
import '../../../models/user_model.dart';
import '../../../services/local/shared_prefs.dart';
import '../../../services/remote/api_client.dart';

class BoardVM extends BaseViewModel {
  List<Board> daily = [];
  List<Board> monthly = [];
  List<Board> all = [];
  List<Board> current = [];
  UserModel userModel = UserModel();
  late Board myAll;
  final boardService = locator<ApiClient>().boardService;
  int viewIndex = 0;
  @override
  void onInit() {}

  BoardVM() {
    getCurrentUser();
    getRank();
  }

  getCurrentUser() {
    var userData = SharedPrefs.getValue('userData');
    userModel = UserModel.fromJson(jsonDecode(userData));
  }

  void getRank() async {
    isLoading = true;
    final res = await boardService.getBoard();
    daily = res.daily.board;
    monthly = res.monthly.board;
    all = res.all.board;
    myAll = daily.firstWhere((element) => element.user.uuid == userModel.uuid);
    current = daily;
    isLoading = false;
    notifyListeners();
  }

  void changeViewIndex() {
    if (viewIndex == 2) {
      viewIndex = 0;
    } else {
      viewIndex++;
    }
    current = viewIndex == 0 ? daily : (viewIndex == 1 ? monthly : all);
    myAll =
        current.firstWhere((element) => element.user.uuid == userModel.uuid);
    notifyListeners();
  }

  @override
  void onAppear() {
    // TODO: implement onAppear
  }

  @override
  void onDisAppear() {
    // TODO: implement onDisAppear
  }
}

class Fake {
  final int rank;
  final String avatar;
  final String name;
  final double hour;

  Fake(
      {required this.rank,
      required this.name,
      required this.avatar,
      required this.hour});
}

final List<Fake> fakes = [
  // Fake(
  //     rank: 1,
  //     name: 'Mai Hoàng Đức',
  //     avatar:
  //         'https://lh3.googleusercontent.com/a-/ACNPEu8jektNVZI_RUWvmFu7cXhsGDsF7cmkGw3-G0ud=s96-c',
  //     hour: 2.5),
  Fake(
      rank: 2,
      name: 'Nguyễn Quốc Bảo',
      avatar:
          'https://thumbs.dreamstime.com/b/default-avatar-profile-trendy-style-social-media-user-icon-187599373.jpg',
      hour: 0.5),
  Fake(
      rank: 3,
      name: 'Test01',
      avatar:
          'https://icdn.dantri.com.vn/thumb_w/640/2020/12/16/ngam-dan-hot-girl-xinh-dep-noi-bat-nhat-nam-2020-docx-1608126694049.jpeg',
      hour: 0),
  Fake(
      rank: 4,
      name: 'Test02',
      avatar:
          'https://thumbs.dreamstime.com/b/default-avatar-profile-trendy-style-social-media-user-icon-187599373.jpg',
      hour: 0),
];
