import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:studywithme/base/base_page.dart';
import 'package:studywithme/pages/main/chats/chat_vm.dart';
import 'package:studywithme/pages/main/chats/widgets/list_chat_person.dart';
import 'package:studywithme/pages/main/chats/widgets/list_chat_room.dart';

import '../../../components/chat_input_field.dart';
import '../../../components/message_item.dart';

class ChatPage extends StatefulWidget {
  const ChatPage({Key? key}) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> with MixinBasePage<ChatVM> {
  @override
  Widget build(BuildContext context) {
    return builder(
      () => DefaultTabController(
        length: 1,
        child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            // //backgroundColor: const Color(0xff764abc),
            // bottom: const
          ),
          body: Container(
            height: MediaQuery.of(context).size.height - kToolbarHeight,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                TabBar(tabs: [
                  Container(
                    height: 40,
                    child: Center(
                      child: Text(
                        'Channels',
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                  ),
                  // Container(
                  //   height: 40,
                  //   child: Center(
                  //     child: Text(
                  //       'Chats',
                  //       style: TextStyle(color: Colors.black),
                  //     ),
                  //   ),
                  // )
                ]),
                Expanded(
                  child: SizedBox(
                    // height: MediaQuery.of(context).size.height * 0.75,
                    child: TabBarView(
                      children: [
                        ListChatRoom(
                          channels: provider.channels,
                        ),
                        // ListChatPerson(
                        //   contacts: provider.contacts,
                        // )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          // body: Padding(
          //   padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
          //   child: provider.isOnRoom
          //       ? Column(
          //           children: [
          //             Expanded(
          //                 child: Stack(
          //               alignment: Alignment.center,
          //               children: [
          //                 Positioned.fill(
          //                     child: ListView.builder(
          //                         controller: provider.scrollController,
          //                         itemCount: provider.messages.length,
          //                         itemBuilder: (context, index) => MessageItem(
          //                             message: provider.messages[index]))),
          //               ],
          //             )),
          //             ChatInputField<ChatVM>(
          //               messageController: provider.messageController,
          //               message: provider.message,
          //               onChanged: provider.onChangeMessage,
          //             ),
          //           ],
          //         )
          //       : Container(),
          // )
        ),
      ),
    );
  }

  @override
  ChatVM create() => ChatVM();

  @override
  void initialise(BuildContext context) {
    // TODO: implement initialise
  }
}
