import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:studywithme/base/base_viewmodel.dart';
import 'package:studywithme/models/chat_channel.dart';
import 'package:studywithme/models/user_model.dart';

import '../../../base/di/locator.dart';
import '../../../constants/socket_event.dart';
import '../../../models/message.dart';
import '../../../models/room.dart';
import '../../../services/remote/api_client.dart';
import '../../../services/remote/socket_client.dart';

class ChatVM extends BaseViewModel {
  final socketService = locator<StreamSocket>().socket;
  late Room room;
  final TextEditingController messageController = TextEditingController();
  final ScrollController scrollController = ScrollController();
  late String message = '';
  List<Message> messages = [];

  final chatService = locator<ApiClient>().chatService;
  final authService = locator<ApiClient>().authService;
  List<ChatChannel> channels = [];
  List<UserModel> contacts = [];

  bool isOnRoom = false;

  ChatVM() {
    getAllChatChannel();
    getAllUser();
  }

  @override
  void onAppear() {
    // TODO: implement onAppear
  }

  @override
  void onDisAppear() {
    // TODO: implement onDisAppear
  }

  @override
  void onInit() {
    // TODO: implement onInit
  }

  void onChangeMessage(message) {
    //messageController.text = message;
    notifyListeners();
  }

  void sendMessage(message) async {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);
    var messageJson = {
      'roomId': room.id,
      'channelId': room.id,
      'type': 'meeting',
      'message': message,
      'time': formattedDate,
      'isSender': true
    };
    socketService?.emit(SocketEvent.sendMessage, {'data': messageJson});
    messages.add(Message.fromJson(messageJson));
    notifyListeners();
  }

  void emptyMessage() {
    messageController.text = '';
    notifyListeners();
  }

  void getAllChatChannel() async {
    final res = await chatService.getChannelChat('static_channel');
    channels = res.data;
    channels =
        channels.where((element) => !(element.isDeleted ?? false)).toList();
    notifyListeners();
  }

  void getAllUser() async {
    final res = await authService.getAllUsers();
    contacts = res.data;
    notifyListeners();
  }
}
