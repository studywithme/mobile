import 'package:flutter/material.dart';
import 'package:studywithme/base/base_page.dart';
import 'package:studywithme/models/chat_channel.dart';
import 'package:studywithme/pages/main/chats/room_chat/room_chat_vm.dart';

import '../../../../components/chat_input_field.dart';
import '../../../../components/message_item.dart';
import 'group_chat_vm.dart';

class GroupChat extends StatefulWidget {
  final ChatChannel channel;
  const GroupChat({Key? key, required this.channel}) : super(key: key);

  @override
  _RoomChatState createState() => _RoomChatState();
}

class _RoomChatState extends State<GroupChat> with MixinBasePage<GroupChatVM> {
  @override
  Widget build(BuildContext context) {
    return builder(() => Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0.0,
            backgroundColor: Colors.white,
            leading: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: const Icon(
                Icons.arrow_back,
                color: Colors.black,
              ),
            ),
            title: Text(
              '#${widget.channel.name}',
              style: const TextStyle(color: Colors.black),
            ),
          ),
          body: Padding(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
            child: Column(
              children: [
                Expanded(
                    child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned.fill(
                        child: ListView.builder(
                            controller: provider.scrollController,
                            itemCount: provider.messages.length,
                            itemBuilder: (context, index) => MessageItem(
                                message: provider.messages[index]))),
                  ],
                )),
                ChatInputField<GroupChatVM>(
                  messageController: provider.messageController,
                  message: provider.message,
                  onChanged: provider.onChangeMessage,
                  callBack: () {
                    provider.sendMessage(provider.messageController.text);
                    provider.scrollController.animateTo(
                        provider.scrollController.position.maxScrollExtent,
                        duration: const Duration(milliseconds: 200),
                        curve: Curves.easeInOut);
                    FocusManager.instance.primaryFocus?.unfocus();
                    provider.emptyMessage();
                  },
                  onSendImage: (file) async {
                    provider.sendImage(file);
                  },
                ),
              ],
            ),
          ),
        ));
  }

  @override
  GroupChatVM create() => GroupChatVM();

  @override
  void initialise(BuildContext context) {
    // provider.initRoom(widget.room, widget.messages);
    provider.initChannel(widget.channel);
  }
}
