import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:studywithme/base/base_viewmodel.dart';
import 'package:studywithme/models/chat_channel.dart';

import '../../../../base/di/locator.dart';
import '../../../../constants/socket_event.dart';
import '../../../../models/message.dart';
import '../../../../models/room.dart';
import '../../../../models/study_detail.dart';
import '../../../../models/user_model.dart';
import '../../../../services/local/shared_prefs.dart';
import '../../../../services/remote/api_client.dart';
import '../../../../services/remote/socket_client.dart';

class GroupChatVM extends BaseViewModel {
  final socketService = locator<StreamSocket>().socket;
  final TextEditingController messageController = TextEditingController();
  final ScrollController scrollController = ScrollController();
  UserModel userModel = UserModel();
  late String message = '';
  List<Message> messages = [];
  late ChatChannel channel;

  final chatService = locator<ApiClient>().chatService;

  bool isOnRoom = false;

  void initChannel(ChatChannel channel) {
    this.channel = channel;
    var userData = SharedPrefs.getValue('userData');
    debugPrint(userData);
    userModel = UserModel.fromJson(jsonDecode(userData));
    listenSocketEvents();
    getMessage();
    notifyListeners();
  }

  @override
  void onAppear() {
    // TODO: implement onAppear
  }

  @override
  void onDisAppear() {
    // TODO: implement onDisAppear
  }

  @override
  void onInit() {
    // TODO: implement onInit
  }

  void onChangeMessage(message) {
    //messageController.text = message;
    notifyListeners();
  }

  void sendMessage(message) async {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);
    var messageJson = {
      'roomId': channel.id,
      'channelId': channel.id,
      'type': 'static_channel',
      'messageType': 'text',
      'message': message,
      'time': formattedDate,
      'isSender': true
    };
    socketService?.emit(SocketEvent.sendMessage, {'data': messageJson});
    messages.add(Message.fromJson(messageJson));
    notifyListeners();
  }

  void emptyMessage() {
    messageController.text = '';
    notifyListeners();
  }

  void getMessage() async {
    isLoading = true;
    try {
      // todo fix get by id
      var res = await chatService.getChannelChatByName(channel.name);
      messages = res.data.messages ?? [];
      //messages.sort((e1, e2) {return e1.id > e2.id;});
    } on DioError catch (e) {
      debugPrint(e.message);
    }
    isLoading = false;
    notifyListeners();
  }

  void sendImage(File file) async {
    final bytes = file.readAsBytesSync();
    String img64 = base64Encode(bytes);
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);
    var messageJson = {
      'roomId': channel.id,
      'channelId': channel.id,
      'type': 'static_channel',
      'messageType': 'image',
      'message': img64,
      'time': formattedDate,
      'isSender': true
    };
    socketService?.emit(SocketEvent.sendMessage, {'data': messageJson});
    messageJson = {
      'roomId': channel.id,
      'channelId': channel.id,
      'type': 'static_channel',
      'messageType': 'image',
      'message': file.path,
      'time': formattedDate,
      'isSender': true,
      'isFirst': true
    };
    messages.add(Message.fromJson(messageJson));
    notifyListeners();
  }

  void listenSocketEvents() async {
    // join chat
    socketService?.emit(SocketEvent.joinChannel, {
      'data': {
        'roomId': channel.id,
        'channelId': channel.id,
        'type': 'static_channel'
      }
    });

    socketService?.on(SocketEvent.userSentMessage, (data) {
      var messageJson = jsonDecode(data)['data'];
      var message = Message.fromJson(messageJson);
      debugPrint("Message ${jsonEncode(messageJson)}");
      if (message.user?.uuid == userModel.uuid) {
        message.isSender = true;
      } else {
        message.isSender = false;
      }
      messages.add(message);
      notifyListeners();
    });
  }
}
