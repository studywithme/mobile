import 'package:flutter/material.dart';
import 'package:studywithme/base/base_page.dart';
import 'package:studywithme/pages/main/chats/room_chat/room_chat_vm.dart';

import '../../../../components/chat_input_field.dart';
import '../../../../components/message_item.dart';
import '../../../../models/message.dart';
import '../../../../models/room.dart';

class RoomChat extends StatefulWidget {
  final Room room;
  final List<Message> messages;
  const RoomChat({Key? key, required this.room, required this.messages})
      : super(key: key);

  @override
  _RoomChatState createState() => _RoomChatState();
}

class _RoomChatState extends State<RoomChat> with MixinBasePage<RoomChatVM> {
  @override
  Widget build(BuildContext context) {
    return builder(() => Padding(
          padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
          child: Column(
            children: [
              Expanded(
                  child: Stack(
                alignment: Alignment.center,
                children: [
                  Positioned.fill(
                      child: ListView.builder(
                          controller: provider.scrollController,
                          itemCount: provider.messages.length,
                          itemBuilder: (context, index) =>
                              MessageItem(message: provider.messages[index]))),
                ],
              )),
              ChatInputField<RoomChatVM>(
                messageController: provider.messageController,
                message: provider.message,
                onChanged: provider.onChangeMessage,
                callBack: () {
                  provider.sendMessage(provider.messageController.text);
                  provider.scrollController.animateTo(
                      provider.scrollController.position.maxScrollExtent,
                      duration: const Duration(milliseconds: 200),
                      curve: Curves.easeInOut);
                  FocusManager.instance.primaryFocus?.unfocus();
                  provider.emptyMessage();
                },
                onSendImage: (file) async {
                  provider.sendImage(file);
                },
              ),
            ],
          ),
        ));
  }

  @override
  RoomChatVM create() => RoomChatVM();

  @override
  void initialise(BuildContext context) {
    provider.initRoom(widget.room, widget.messages);
  }
}
