import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:studywithme/base/base_viewmodel.dart';
import 'package:studywithme/base/di/locator.dart';
import 'package:studywithme/services/remote/api_client.dart';

import '../../../../constants/socket_event.dart';
import '../../../../models/message.dart';
import '../../../../models/room.dart';
import '../../../../services/remote/socket_client.dart';

class RoomChatVM extends BaseViewModel {
  final socketService = locator<StreamSocket>().socket;
  late Room room;
  final TextEditingController messageController = TextEditingController();
  final ScrollController scrollController = ScrollController();
  late String message = '';
  List<Message> messages = [];

  final chatService = locator<ApiClient>().chatService;

  @override
  void onInit() {
    //listenSocketEvents();
  }

  void initRoom(Room room, List<Message> messages) {
    this.room = room;
    this.messages = messages;
  }

  // void getMessage() async {
  //   try {
  //     var res = await chatService.getChannelChatByName('chat_room${room.id}');
  //     messages = res.data.messages!;
  //   } on DioError catch (e) {
  //     debugPrint(e.message);
  //   }
  //   notifyListeners();
  // }

  void onEditCompleted() {
    messageController.text = messageController.text;
    notifyListeners();
  }

  void sendMessage(message) async {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);
    var messageJson = {
      'roomId': room.id,
      'channelId': room.id,
      'type': 'meeting',
      'messageType': 'text',
      'message': message,
      'time': formattedDate,
      'isSender': true
    };
    socketService?.emit(SocketEvent.sendMessage, {'data': messageJson});
    messages.add(Message.fromJson(messageJson));
    notifyListeners();
  }

  void emptyMessage() {
    messageController.text = '';
    notifyListeners();
  }

  void onChangeMessage(message) {
    //messageController.text = message;
    notifyListeners();
  }

  void listenSocketEvents() async {
    socketService?.on(SocketEvent.userSentMessage, (data) {
      var messageJson = jsonDecode(data)['data'];
      var message = Message.fromJson(messageJson);
      messages.add(message);
      notifyListeners();
    });
  }

  void sendImage(File file) async {
    final bytes = file.readAsBytesSync();
    String img64 = base64Encode(bytes);
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);
    var messageJson = {
      'roomId': room.id,
      'channelId': room.id,
      'type': 'meeting',
      'messageType': 'image',
      'message': img64,
      'time': formattedDate,
      'isSender': true
    };

    socketService?.emit(SocketEvent.sendMessage, {'data': messageJson});
    messageJson = {
      'roomId': room.id,
      'channelId': room.id,
      'type': 'meeting',
      'messageType': 'image',
      'message': img64,
      'time': formattedDate,
      'isSender': true,
      'isFirst': true
    };
    messages.add(Message.fromJson(messageJson));
    notifyListeners();
  }

  @override
  void onAppear() {
    // TODO: implement onAppear
  }

  @override
  void onDisAppear() {
    // TODO: implement onDisAppear
  }
}
