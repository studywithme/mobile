import 'package:flutter/material.dart';
import 'package:studywithme/base/base_page.dart';
import 'package:studywithme/pages/main/chats/single_chat/single_chat_vm.dart';

import '../../../../components/chat_input_field.dart';
import '../../../../components/message_item.dart';

class SingleChat extends StatefulWidget {
  const SingleChat({Key? key}) : super(key: key);

  @override
  _RoomChatState createState() => _RoomChatState();
}

class _RoomChatState extends State<SingleChat>
    with MixinBasePage<SingleChatVM> {
  @override
  Widget build(BuildContext context) {
    return builder(() => Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0.0,
            backgroundColor: Colors.white,
            leading: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: const Icon(
                Icons.arrow_back,
                color: Colors.black,
              ),
            ),
            title: Text(
              'Nguyễn Quốc Bảo',
              style: const TextStyle(color: Colors.black),
            ),
          ),
          body: Padding(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
            child: Column(
              children: [
                Expanded(
                    child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned.fill(
                        child: ListView.builder(
                            controller: provider.scrollController,
                            itemCount: provider.messages.length,
                            itemBuilder: (context, index) => MessageItem(
                                message: provider.messages[index]))),
                  ],
                )),
                ChatInputField<SingleChatVM>(
                  messageController: provider.messageController,
                  message: provider.message,
                  onChanged: provider.onChangeMessage,
                ),
              ],
            ),
          ),
        ));
  }

  @override
  SingleChatVM create() => SingleChatVM();

  @override
  void initialise(BuildContext context) {
    // provider.initRoom(widget.room, widget.messages);
  }
}
