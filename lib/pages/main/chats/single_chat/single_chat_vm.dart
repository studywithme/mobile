import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:studywithme/base/base_viewmodel.dart';

import '../../../../base/di/locator.dart';
import '../../../../constants/socket_event.dart';
import '../../../../models/message.dart';
import '../../../../models/room.dart';
import '../../../../models/user_model.dart';
import '../../../../services/remote/api_client.dart';
import '../../../../services/remote/socket_client.dart';

class SingleChatVM extends BaseViewModel {
  final socketService = locator<StreamSocket>().socket;
  late UserModel target;
  late UserModel current;
  final TextEditingController messageController = TextEditingController();
  final ScrollController scrollController = ScrollController();
  late String message = '';
  List<Message> messages = [];

  final chatService = locator<ApiClient>().chatService;

  bool isOnRoom = false;
  @override
  void onAppear() {
    // TODO: implement onAppear
  }

  @override
  void onDisAppear() {
    // TODO: implement onDisAppear
  }

  @override
  void onInit() {
    // TODO: implement onInit
  }

  void onChangeMessage(message) {
    //messageController.text = message;
    notifyListeners();
  }

  void sendMessage(message) async {
    // DateTime now = DateTime.now();
    // String formattedDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);
    // var messageJson = {
    //   'roomId': room.id,
    //   'channelId': room.id,
    //   'type': 'meeting',
    //   'message': message,
    //   'time': formattedDate,
    //   'isSender': true
    // };
    // socketService?.emit(SocketEvent.sendMessage, {'data': messageJson});
    // messages.add(Message.fromJson(messageJson));
    notifyListeners();
  }

  void emptyMessage() {
    messageController.text = '';
    notifyListeners();
  }

  void getMessage() async {
    // try {
    //   var res = await chatService.getChannelChatByName('channel_single_${room.id}');
    //   messages = res.data.messages ?? [];
    // } on DioError catch (e) {
    //   debugPrint(e.message);
    // }
    notifyListeners();
  }
}
