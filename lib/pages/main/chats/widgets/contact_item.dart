import 'package:flutter/material.dart';
import 'package:studywithme/models/user_model.dart';
import 'package:studywithme/pages/main/boards/board_vm.dart';
import '../../../../utils/converter.dart';

class ContactItem extends StatelessWidget {
  final UserModel contact;
  const ContactItem({Key? key, required this.contact}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      height: 80,
      child: Stack(
        children: [
          Positioned.fill(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CircleAvatar(
                  radius: 30,
                  backgroundImage: NetworkImage(contact.avatar ?? ''),
                ),
                const SizedBox(
                  width: 10,
                ),
                Flexible(
                  child: Container(
                    margin: const EdgeInsets.only(right: 80),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          contact.email.username,
                          style: const TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 18),
                        ),
                        // const SizedBox(
                        //   height: 12,
                        // ),
                        // // const Text(
                        // //   'Hello, evening too Andrew Hello, evening too Andrew',
                        // //   style: TextStyle(fontSize: 15, color: Colors.grey),
                        // //   overflow: TextOverflow.ellipsis,
                        // //   maxLines: 1,
                        // // ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          Visibility(
            visible: false,
            child: Positioned(
                right: 10,
                top: 25,
                child: Container(
                  height: 22,
                  width: 22,
                  decoration: const BoxDecoration(
                      color: Colors.deepPurpleAccent, shape: BoxShape.circle),
                  child: const Center(
                    child: Text(
                      '1',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                )),
          )
        ],
      ),
    );
  }
}
