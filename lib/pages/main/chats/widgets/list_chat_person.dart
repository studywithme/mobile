import 'package:flutter/material.dart';
import 'package:studywithme/models/chat_channel.dart';
import 'package:studywithme/models/user_model.dart';
import 'package:studywithme/pages/main/boards/board_vm.dart';
import 'package:studywithme/pages/main/chats/widgets/contact_item.dart';

import '../single_chat/single_chat.dart';

class ListChatPerson extends StatefulWidget {
  final List<UserModel> contacts;
  const ListChatPerson({Key? key, required this.contacts}) : super(key: key);

  @override
  _ListChatPersonState createState() => _ListChatPersonState();
}

class _ListChatPersonState extends State<ListChatPerson> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.8,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView.builder(
            itemCount: widget.contacts.length,
            itemBuilder: (context, index) => Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SingleChat()));
                      },
                      child: ContactItem(
                        contact: widget.contacts[index],
                      )),
                )),
      ),
    );
  }
}
