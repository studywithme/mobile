import 'package:flutter/material.dart';
import 'package:studywithme/pages/main/chats/chat_vm.dart';
import 'package:studywithme/pages/main/chats/group_chat/group_chat.dart';

import '../../../../models/chat_channel.dart';

class ListChatRoom extends StatefulWidget {
  final List<ChatChannel> channels;
  const ListChatRoom({Key? key, required this.channels}) : super(key: key);

  @override
  _ListChatRoomState createState() => _ListChatRoomState();
}

class _ListChatRoomState extends State<ListChatRoom> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.8,
      child: ListView.builder(
          itemCount: widget.channels.length,
          itemBuilder: (context, index) => InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => GroupChat(
                                channel: widget.channels[index],
                              )));
                },
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  height: 40,
                  margin: const EdgeInsets.only(bottom: 5),
                  decoration:
                      BoxDecoration(color: Colors.white.withOpacity(0.6)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '# ${widget.channels[index].name}',
                        style: TextStyle(
                            fontSize: 18, color: Colors.black.withOpacity(0.5)),
                      ),
                    ],
                  ),
                ),
              )),
    );
  }
}
