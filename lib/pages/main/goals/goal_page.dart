import 'package:flutter/material.dart';
import 'package:studywithme/base/base_page.dart';
import 'package:studywithme/pages/main/goals/goal_vm.dart';
import 'package:studywithme/utils/colors.dart';

import '../../../components/custom_goal.dart';
import '../../../components/custom_input.dart';
import '../../../components/room_button.dart';

class GoalPage extends StatefulWidget {
  const GoalPage({Key? key}) : super(key: key);

  @override
  _GoalPageState createState() => _GoalPageState();
}

class _GoalPageState extends State<GoalPage> with MixinBasePage<GoalVM> {
  @override
  Widget build(BuildContext context) {
    return builder(() => Scaffold(
          backgroundColor: AppColor.hf4f2f2,
          body: Container(
            height: MediaQuery.of(context).size.height,
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            child: Wrap(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    // Row(
                    //   children: const [
                    //     Icon(Icons.flag),
                    //     SizedBox(
                    //       width: 5,
                    //     ),
                    //     Text(
                    //       'Session Goals',
                    //       style: TextStyle(fontSize: 18),
                    //     )
                    //   ],
                    // ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.75,
                          height: 60,
                          child: CustomInput(
                            focusNode: provider.focusNode,
                            controller: provider.goalController,
                            placeHolder: 'Type a goal...',
                            backgroundColor: Colors.white,
                            hintStyle: const TextStyle(color: Colors.black),
                          ),
                        ),
                        const Spacer(),
                        RoomButton(
                            onTap: provider.createGoal,
                            icon: Icon(
                              Icons.add,
                              color: provider.goalController.text.isNotEmpty
                                  ? Colors.deepPurpleAccent
                                  : Colors.white,
                            ))
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 80,
                      decoration: BoxDecoration(
                          color: Colors.black.withOpacity(0.6),
                          borderRadius: BorderRadius.circular(15)),
                      width: double.infinity,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          InkWell(
                            onTap: () {
                              provider.changePage(true);
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  '${provider.open}',
                                  style: const TextStyle(
                                      fontSize: 30, color: Colors.white),
                                ),
                                const Text('Open',
                                    style: TextStyle(color: Colors.white))
                              ],
                            ),
                          ),
                          const SizedBox(
                            width: 30,
                          ),
                          const VerticalDivider(
                            width: 20,
                            thickness: 1,
                            indent: 5,
                            endIndent: 1,
                            color: Colors.grey,
                          ),
                          const SizedBox(
                            width: 30,
                          ),
                          InkWell(
                            onTap: () {
                              provider.changePage(false);
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  '${provider.completed}',
                                  style: const TextStyle(
                                      fontSize: 30, color: Colors.greenAccent),
                                ),
                                const Text(
                                  'Completed',
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    provider.isOpenPage
                        ? ConstrainedBox(
                            constraints: const BoxConstraints(maxHeight: 400),
                            child: ListView.builder(
                                itemCount: provider.openGoals.length,
                                itemBuilder: (context, index) => Dismissible(
                                      key: UniqueKey(),
                                      background: Container(),
                                      secondaryBackground: Container(
                                        color: Colors.red,
                                        child: Padding(
                                          padding: const EdgeInsets.all(15),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: const [
                                              Icon(Icons.delete,
                                                  color: Colors.white),
                                            ],
                                          ),
                                        ),
                                      ),
                                      onDismissed:
                                          (DismissDirection direction) {
                                        if (direction ==
                                            DismissDirection.startToEnd) {
                                        } else {
                                          provider.deleteGoal(
                                              provider.openGoals[index].id!);
                                        }
                                      },
                                      child: CustomGoal(
                                        goal: provider.openGoals[index],
                                        onEdit: provider.updateGoal,
                                        onChanged: (value) {
                                          provider.openGoals[index].status =
                                              provider.openGoals[index]
                                                          .status ==
                                                      'open'
                                                  ? 'completed'
                                                  : 'open';
                                          provider.updateStatus(
                                              provider.openGoals[index]);
                                        },
                                      ),
                                    )),
                          )
                        : ConstrainedBox(
                            constraints: const BoxConstraints(maxHeight: 400),
                            child: ListView.builder(
                                itemCount: provider.completedGoals.length,
                                itemBuilder: (context, index) => Dismissible(
                                      key: UniqueKey(),
                                      background: Container(),
                                      secondaryBackground: Container(
                                        color: Colors.red,
                                        child: Padding(
                                          padding: const EdgeInsets.all(15),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: const [
                                              Icon(Icons.delete,
                                                  color: Colors.white),
                                            ],
                                          ),
                                        ),
                                      ),
                                      onDismissed:
                                          (DismissDirection direction) {
                                        if (direction ==
                                            DismissDirection.startToEnd) {
                                        } else {
                                          provider.deleteGoal(provider
                                              .completedGoals[index].id!);
                                        }
                                      },
                                      child: CustomGoal(
                                        goal: provider.completedGoals[index],
                                        onEdit: provider.updateGoal,
                                        onChanged: (value) {
                                          provider.completedGoals[index]
                                              .status = provider
                                                      .completedGoals[index]
                                                      .status ==
                                                  'open'
                                              ? 'completed'
                                              : 'open';
                                          provider.updateStatus(
                                              provider.completedGoals[index]);
                                        },
                                      ),
                                    )),
                          ),
                  ],
                ),
              ],
            ),
          ),
        ));
  }

  @override
  GoalVM create() => GoalVM();

  @override
  void initialise(BuildContext context) {
    // TODO: implement initialise
  }
}
