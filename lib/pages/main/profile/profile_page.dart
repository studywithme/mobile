import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'package:provider/provider.dart';
import 'package:studywithme/pages/main/profile/profile_vm.dart';
import '../../../base/base_page.dart';
import '../../../components/custom_button.dart';
import '../../../components/custom_setting.dart';
import '../../../gen/assets.gen.dart';
import '../../../generated/l10n.dart';
import '../../../services/local/shared_prefs.dart';
import '../../../utils/colors.dart';
import '../profile_detail/profile_detail.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>
    with MixinBasePage<ProfileViewModel> {
  @override
  Widget build(BuildContext context) {
    return builder(() => Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0.0,
            backgroundColor: Colors.white,
            automaticallyImplyLeading: false,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                    height: 30,
                    width: 30,
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            fit: BoxFit.cover, image: AssetImage('')))),
                const SizedBox(
                  width: 10,
                ),
                const Text(
                  '',
                  style: TextStyle(color: Colors.black),
                ),
              ],
            ),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                children: [
                  InkWell(
                    onTap: () async {
                      XFile? file = await ImagePicker().pickImage(
                          source: ImageSource.gallery,
                          maxHeight: 1800,
                          maxWidth: 1800);
                      if (file != null) {
                        File selectedImage = File(file.path);
                        await provider.updateAvatar(selectedImage);
                      }
                    },
                    child: Container(
                      height: 150,
                      width: 150,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(provider.user.avatar ?? ''))),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(provider.user.name ?? provider.user.email),
                  const SizedBox(
                    height: 20,
                  ),
                  // InkWell(
                  //     onTap: () {
                  //       Navigator.push(
                  //           context,
                  //           MaterialPageRoute(
                  //               builder: (context) => const VipPage()));
                  //     },
                  //     child: Assets.images.enjoyBenefit.image()),
                  CustomSetting(
                    icon: const Icon(Icons.settings),
                    label: 'Profile',
                    onTap: () async {
                      final res = await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProfileDetail(
                                    user: provider.user,
                                  )));
                      if (res != null) {
                        provider.updateUser(res);
                      }
                    },
                  ),
                  // SizedBox(
                  //   height: 40,
                  //   child: Row(
                  //     mainAxisAlignment: MainAxisAlignment.end,
                  //     children: [
                  //       const Icon(Icons.visibility_outlined),
                  //       const SizedBox(
                  //         width: 10,
                  //       ),
                  //       Text(S.of(context).dark_mode),
                  //       const Spacer(),
                  //       Switch(
                  //           value: settingProvider.themeMode == ThemeMode.dark,
                  //           onChanged: (value) {
                  //             settingProvider.switchMode(
                  //                 value ? ThemeMode.dark : ThemeMode.light);
                  //           })
                  //     ],
                  //   ),
                  // ),
                  // CustomSetting(
                  //   icon: const Icon(Icons.help_center_outlined),
                  //   label: 'Help',
                  //   onTap: () {},
                  // ),
                  // CustomSetting(
                  //   icon: const Icon(Icons.group_outlined),
                  //   label: 'Invite friend',
                  //   onTap: () {},
                  // ),
                  CustomSetting(
                    icon: const Icon(
                      Icons.logout,
                      color: Colors.red,
                    ),
                    label: 'Logout',
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                                content: Container(
                                  height: 100,
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 5, horizontal: 10),
                                  decoration: const BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(40),
                                          topRight: Radius.circular(40))),
                                  child: Column(
                                    children: [
                                      const Text('Are you sure to logout'),
                                      const SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          ElevatedButton(
                                              onPressed: () {},
                                              child: const Text('Cancel')),
                                          const SizedBox(
                                            width: 15,
                                          ),
                                          ElevatedButton(
                                              onPressed: () async {
                                                await SharedPrefs.setValue(
                                                    'token', '');
                                                if (mounted) {
                                                  Navigator.pop(context);
                                                }
                                              },
                                              child: const Text('OK')),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ));
                    },
                    hasNavigate: false,
                    customStyle: const TextStyle(color: Colors.red),
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  @override
  ProfileViewModel create() => ProfileViewModel();
  @override
  void initialise(BuildContext context) {
    // TODO: implement initialise
  }
}
