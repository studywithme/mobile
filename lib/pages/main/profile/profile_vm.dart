import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:studywithme/base/base_viewmodel.dart';
import 'package:studywithme/base/di/locator.dart';
import 'package:studywithme/models/user_model.dart';
import 'package:studywithme/services/remote/api_client.dart';

import '../../../services/local/shared_prefs.dart';

class ProfileViewModel extends BaseViewModel {
  late UserModel user;

  final userService = locator<ApiClient>().userService;

  ProfileViewModel() {
    var userData = SharedPrefs.getValue('userData');
    user = UserModel.fromJson(jsonDecode(userData));

    // get profile
    getProfile();
  }

  void getProfile() async {
    try {
      final res = await userService.getProfile();
      user = res.data;
    } on DioError catch (e) {
      debugPrint("Error");
    }
    notifyListeners();
  }

  void updateUser(UserModel userData) {
    user = userData;
    notifyListeners();
  }

  Future<void> updateAvatar(File file) async {
    FormData data = FormData();
    data.files.add(MapEntry(
        'file',
        MultipartFile.fromFileSync(file.path,
            filename: file.path.split(Platform.pathSeparator).last)));
    try {
      final res = await userService.updateAvatar(data);
      if (res.path != null) {
        user.avatar = res.path;
        notifyListeners();
      }
    } catch (e) {}
  }

  @override
  void onAppear() {
    // TODO: implement onAppear
  }

  @override
  void onDisAppear() {
    // TODO: implement onDisAppear
  }

  @override
  void onInit() {
    // TODO: implement onInit
  }
}
