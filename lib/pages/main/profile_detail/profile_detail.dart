import 'package:flutter/material.dart';
import 'package:studywithme/base/base_page.dart';
import 'package:studywithme/pages/main/profile_detail/profile_detail_vm.dart';

import '../../../components/custom_button.dart';
import '../../../components/custom_dropdown.dart';
import '../../../components/custom_input.dart';
import '../../../models/user_model.dart';

class ProfileDetail extends StatefulWidget {
  final UserModel user;
  const ProfileDetail({Key? key, required this.user}) : super(key: key);

  @override
  _ProfileDetailState createState() => _ProfileDetailState();
}

class _ProfileDetailState extends State<ProfileDetail>
    with MixinBasePage<ProfileDetailVM> {
  @override
  Widget build(BuildContext context) {
    return builder(() => Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
              elevation: 0.0,
              backgroundColor: Colors.white,
              leading: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: const Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ),
              ),
              title: const Text(
                'Edit Profile',
                style: TextStyle(color: Colors.black),
              )),
          body: Container(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.76,
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Name',
                            style: TextStyle(fontSize: 18, color: Colors.grey),
                          ),
                          const SizedBox(
                            height: 6,
                          ),
                          CustomInput(
                            controller: provider.nameController,
                            placeHolder: 'Name',
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Text(
                            'Personal Study Time (in minutes)',
                            style: TextStyle(fontSize: 18, color: Colors.grey),
                          ),
                          const SizedBox(
                            height: 6,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          CustomInput(
                            controller: provider.timeController,
                            placeHolder: 'Personal Time (m)',
                            inputType: TextInputType.number,
                          ),
                          // Gender
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                      //height: context.height * 0.08,
                      child: CustomButton(
                          title: 'Save',
                          onPress: () async {
                            final res = await provider.updateProfile();
                            if (res == null) {
                              const snackBar = SnackBar(
                                content: Text(
                                    'Update error! Please check you information!'),
                              );
                              if (!mounted) return;
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackBar);
                            } else {
                              const snackBar = SnackBar(
                                content: Text(
                                    'Update success! You will be redirect to previous page!'),
                              );
                              if (!mounted) return;
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackBar);
                              await Future.delayed(const Duration(seconds: 3))
                                  .then((value) => Navigator.pop(context, res));
                            }
                          }))
                ],
              ),
            ),
          ),
        ));
  }

  @override
  ProfileDetailVM create() => ProfileDetailVM();

  @override
  void initialise(BuildContext context) {
    provider.initData(widget.user);
  }
}
