import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:studywithme/base/base_viewmodel.dart';
import 'package:studywithme/models/user_model.dart';
import 'package:studywithme/services/local/shared_prefs.dart';

import '../../../base/di/locator.dart';
import '../../../services/remote/api_client.dart';

class ProfileDetailVM extends BaseViewModel {
  late UserModel userModel;
  final nameController = TextEditingController();
  final timeController = TextEditingController();

  final userService = locator<ApiClient>().userService;

  String gender = 'Male';
  ProfileDetailVM() {}

  void initData(UserModel user) {
    userModel = user;
    gender = userModel.gender == 0
        ? 'Male'
        : (userModel.gender == 1 ? 'Female' : 'Other');

    nameController.text = userModel.name ?? '';
    timeController.text = '60';
    notifyListeners();
  }

  @override
  void onAppear() {
    // TODO: implement onAppear
  }

  @override
  void onDisAppear() {
    // TODO: implement onDisAppear
  }

  @override
  void onInit() {
    // TODO: implement onInit
  }

  void onSelectGender(value) {
    userModel.gender = value == 'Male' ? 0 : (value == 'Female' ? 1 : 2);
  }

  Future<UserModel?> updateProfile() async {
    try {
      int? studyTime = int.tryParse(timeController.text);
      Map<String, dynamic> data = {
        'name': nameController.text,
        'gender': userModel.gender,
        'studyTime': studyTime
      };
      await SharedPrefs.setValue('studyTime', '${studyTime ?? 60}');
      final res = await userService.updateProfile(data);
      userModel = res.data;
    } catch (e) {
      return null;
    }
    return userModel;
  }
}
