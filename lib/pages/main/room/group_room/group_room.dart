import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:studywithme/base/base_page.dart';
import 'package:studywithme/models/goal.dart';
import 'package:studywithme/pages/main/app_page.dart';
import 'package:studywithme/pages/main/room/group_room/group_room_vm.dart';
import 'package:studywithme/pages/main/room/group_room/widgets/room_management.dart';

import '../../../../components/goal_status.dart';
import '../../../../components/room_button.dart';
import '../../../../components/time_counter.dart';
import '../../../../models/room.dart';
import '../../../../models/room_option.dart';
import '../../chats/room_chat/room_chat.dart';
import '../widgets/session_goal.dart';

import 'package:agora_rtc_engine/rtc_local_view.dart' as rtc_local_view;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as rtc_remote_view;

class GroupRoomPage extends StatefulWidget {
  final Room room;
  final String token;
  final RoomOption setting;
  const GroupRoomPage(
      {Key? key,
      required this.room,
      required this.token,
      required this.setting})
      : super(key: key);

  @override
  _GroupRoomPageState createState() => _GroupRoomPageState();
}

class _GroupRoomPageState extends State<GroupRoomPage>
    with MixinBasePage<GroupRoomVM> {
  @override
  Widget build(BuildContext context) {
    return builder(() => WillPopScope(
          onWillPop: () async => false,
          child: Scaffold(
            body: Stack(
              children: [
                Positioned.fill(
                    child: Container(
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              'https://i.scdn.co/image/ab67616d0000b2735668c6562f6ebc56d2c44d06'))),
                  child: Center(
                    child: CarouselSlider(
                      options: CarouselOptions(
                          enableInfiniteScroll: false,
                          viewportFraction: 1,
                          height: MediaQuery.of(context).size.height * 0.35),
                      items: [
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.9,
                          height: MediaQuery.of(context).size.height * 0.35,
                          child: Stack(
                            children: [
                              Positioned.fill(
                                  child: provider.setting.isCamera
                                      ? const rtc_local_view.SurfaceView(
                                          zOrderMediaOverlay: true,
                                          zOrderOnTop: true,
                                        )
                                      : Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.9,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.35,
                                          decoration: BoxDecoration(
                                              color: Colors.black54,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Center(
                                            child: Container(
                                              height: 80,
                                              width: 80,
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  image: DecorationImage(
                                                      fit: BoxFit.cover,
                                                      image: NetworkImage(provider
                                                              .userModel
                                                              .avatar ??
                                                          'https://banner2.cleanpng.com/20180402/ojw/kisspng-united-states-avatar-organization-information-user-avatar-5ac20804a62b58.8673620215226654766806.jpg'))),
                                            ),
                                          ),
                                        )),
                              Positioned(
                                bottom: 0,
                                left: 0,
                                child: Container(
                                  width: 150,
                                  height: 40,
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 2, horizontal: 3),
                                  decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(10)),
                                      color: Colors.black45),
                                  child: Center(
                                    child: Text(
                                      provider.userModel.email,
                                      style: TextStyle(
                                          color: Colors.white.withOpacity(0.8),
                                          overflow: TextOverflow.clip),
                                      maxLines: 3,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        ...List.of(provider.currentUsers.map(
                          (e) => SizedBox(
                            height: MediaQuery.of(context).size.height * 0.35,
                            width: MediaQuery.of(context).size.width * 0.9,
                            child: Stack(
                              children: [
                                Positioned.fill(
                                    child: (e.studySession.isCamera ?? false)
                                        ? rtc_remote_view.TextureView(
                                            uid: e.id,
                                            channelId: provider.channelName,
                                          )
                                        : Container()),
                                Positioned(
                                    top: 0,
                                    left: 0,
                                    child: Visibility(
                                      visible:
                                          !(e.studySession.isCamera ?? false),
                                      child: Container(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.9,
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.35,
                                        decoration: BoxDecoration(
                                            color: Colors.black54,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: Center(
                                          child: Container(
                                            height: 80,
                                            width: 80,
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                image: DecorationImage(
                                                    image: NetworkImage(
                                                        e.avatar ?? ''))),
                                          ),
                                        ),
                                      ),
                                    )),
                                Positioned(
                                  bottom: 0,
                                  left: 0,
                                  child: Container(
                                    width: 150,
                                    height: 40,
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 2, horizontal: 3),
                                    decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(10)),
                                        color: Colors.black45),
                                    child: Center(
                                      child: Text(
                                        e.email,
                                        style: TextStyle(
                                            color:
                                                Colors.white.withOpacity(0.8),
                                            overflow: TextOverflow.clip),
                                        maxLines: 3,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ))
                      ],
                    ),
                  ),
                )),
                Positioned(
                    top: 20,
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 5),
                      height: 120,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              TimeCounter(
                                controller: provider.timeController,
                                isOnBreak: provider.isOnBreak,
                                isPersonal: false,
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          InkWell(
                              onTap: () {
                                List<Goal> goals = provider.goals;
                                showModalBottomSheet(
                                  isScrollControlled: true,
                                  context: context,
                                  backgroundColor: Colors.white,
                                  shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadiusDirectional.only(
                                      topEnd: Radius.circular(25),
                                      topStart: Radius.circular(25),
                                    ),
                                  ),
                                  builder: (context) => SingleChildScrollView(
                                    padding: const EdgeInsetsDirectional.only(
                                      start: 20,
                                      end: 20,
                                      bottom: 30,
                                      top: 8,
                                    ),
                                    child: SessionGoalWidget(
                                      goals: provider.goals,
                                    ),
                                  ),
                                );
                              },
                              child: GoalStatus(
                                goals: provider.goals,
                              ))
                        ],
                      ),
                    )),
                // Positioned(
                //     top: 30,
                //     right: 20,
                //     child: RoomButton(
                //       icon: Icon(
                //         provider.isShowOption ? Icons.close : Icons.more_vert,
                //         color: Colors.white,
                //       ),
                //       onTap: provider.showOption,
                //     )),
                // Positioned(
                //   top: 95,
                //   right: 20,
                //   child: Visibility(
                //     visible: provider.isShowOption,
                //     child: SizedBox(
                //       height: 200,
                //       width: 55,
                //       child: ListView(
                //         children: [
                //           RoomButton(
                //             icon: const Icon(
                //               Icons.photo,
                //               color: Colors.white,
                //             ),
                //             onTap: () {},
                //           ),
                //           const SizedBox(
                //             height: 10,
                //           ),
                //           RoomButton(
                //             icon: const Icon(
                //               Icons.music_note,
                //               color: Colors.white,
                //             ),
                //             onTap: () {},
                //           )
                //         ],
                //       ),
                //     ),
                //   ),
                // ),
                Positioned(
                    bottom: 120,
                    left: 10,
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width * 0.9,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          RoomButton(
                            icon: Icon(
                              provider.setting.isCamera
                                  ? Icons.videocam_rounded
                                  : Icons.videocam_off_rounded,
                              color: Colors.white,
                            ),
                            onTap: provider.turnOnCamera,
                          ),
                          RoomButton(
                            icon: Icon(
                              provider.setting.isMicro
                                  ? Icons.mic
                                  : Icons.mic_off,
                              color: Colors.white,
                            ),
                            onTap: provider.turnOnMicro,
                          ),
                          RoomButton(
                            icon: const Icon(
                              Icons.group_outlined,
                              color: Colors.white,
                            ),
                            onTap: () {
                              showChats(provider, 0);
                            },
                          ),
                          RoomButton(
                            icon: const Icon(
                              Icons.chat,
                              color: Colors.white,
                            ),
                            onTap: () {
                              showChats(provider, 1);
                            },
                          ),
                          // RoomButton(
                          //   icon: const Icon(
                          //     Icons.report,
                          //     color: Colors.red,
                          //   ),
                          //   onTap: () {},
                          // )
                        ],
                      ),
                    )),
                Positioned(
                    bottom: 20,
                    right: 20,
                    child: RoomButton(
                      icon: const Icon(
                        Icons.exit_to_app,
                        color: Colors.red,
                      ),
                      onTap: () {
                        showConfirmLeaveRoom();
                        //Navigator.pop(context);
                      },
                    ))
              ],
            ),
          ),
        ));
  }

  void showChats(GroupRoomVM model, int index) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        backgroundColor: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadiusDirectional.only(
            topEnd: Radius.circular(25),
            topStart: Radius.circular(25),
          ),
        ),
        builder: (context) => ChangeNotifierProvider.value(
              value: model,
              child: RoomManagement(
                room: provider.room,
                messages: provider.messages,
                initIndex: index,
              ),
            ));
  }

  showConfirmLeaveRoom() {
    showDialog(
        context: context,
        builder: (dialogContext) => AlertDialog(
              content: const Text(
                  'Are you sure you want to end your study session?'),
              actions: [
                TextButton(
                    onPressed: () async {
                      await provider.leaveChannel();
                      if (!mounted) {
                        return;
                      }
                      provider.dispose();
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const AppPage()));
                    },
                    child: const Text('Leave call')),
                TextButton(
                    onPressed: () {
                      Navigator.pop(dialogContext);
                    },
                    child: const Text('Stay in session'))
              ],
            ));
  }

  @override
  GroupRoomVM create() => GroupRoomVM();

  @override
  void initialise(BuildContext context) {
    provider.initContext(context, mounted);
    provider.initRoom(widget.room, widget.token, widget.setting);
  }

  @override
  void dispose() {
    debugPrint("DISPOSE");
    provider.leaveChannel();
    provider.dispose();
    super.dispose();
  }
}
