import 'dart:async';
import 'dart:convert';
import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/countdown_timer_controller.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:studywithme/base/base_viewmodel.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:studywithme/constants/socket_event.dart';
import 'package:studywithme/models/goal.dart';
import 'package:studywithme/models/joined_user.dart';
import 'package:studywithme/models/room_option.dart';
import 'package:studywithme/models/stream/chat_stream.dart';
import 'package:studywithme/models/stream/room_stream.dart';
import 'package:studywithme/models/study_detail.dart';
import 'package:studywithme/models/user_model.dart';
import 'package:studywithme/services/local/shared_prefs.dart';
import 'package:studywithme/services/remote/socket_client.dart';

import '../../../../base/di/locator.dart';
import '../../../../constants/app_constants.dart';
import '../../../../models/message.dart';
import '../../../../models/room.dart';
import '../../../../services/remote/api_client.dart';

class GroupRoomVM extends BaseViewModel {
  final RoomStream roomStream = locator<RoomStream>();
  final MessageStream messageStream = locator<MessageStream>();

  late CountdownTimerController groupTimeController;
  late CountdownTimerController personalTimeController;
  late CountdownTimerController breakTimeController;
  late CountdownTimerController timeController;

  late Room room;
  late List<StudyDetail> currentUsers = [];
  late List<StudyDetail> allUsers = [];
  late List<Goal> goals = [];
  // late StudyDetail myAccount = StudyDetail();
  UserModel userModel = UserModel();
  RoomOption setting = RoomOption();
  bool isConnected = false;

  bool isShowOption = false;

  late final RtcEngine engine;
  bool isJoined = false,
      isSwitchCamera = true,
      isSwitchRender = true,
      hasAudio = true,
      hasVolume = true,
      hasCamera = true;

  List<int> remoteUid = [];
  String token = '';
  String channelName = 'test1';

  late BuildContext context;
  late bool mounted = false;
  final socketService = locator<StreamSocket>().socket;
  final chatService = locator<ApiClient>().chatService;
  final goalService = locator<ApiClient>().goalService;
  List<Message> messages = [];
  bool isOnBreak = false;
  bool isPersonalTime = false;
  int groupMinutes = 60;
  int personalMinutes = 1;
  int breakMinutes = 1;

  int remainGroupTime = 0;
  int remainBreakTime = 0;
  int remainPersonTime = 0;

  late int endGroupTime;
  late int endBreakTime;
  // late Timer _groupTimer;
  // late Timer _personalTimer;

  @override
  void onInit() {
    showLoading();
    listenSocketEvents();
    initEngine();
    hideLoading();
    getMessage();
    getGoals();
    initTimeController();
    notifyListeners();
  }

  void initContext(BuildContext context, bool mounted) {
    this.context = context;
    this.mounted = mounted;
  }

  void initRoom(Room room, String token, RoomOption setting) {
    this.room = room;
    this.token = token;
    this.setting = setting;
    if (room.lastedJoinTime != null) {
      DateFormat dateTimeFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
      DateTime dateTime = dateTimeFormat.parse(room.lastedJoinTime!);
      DateTime now = DateTime.now();
      int remain = (now.difference(dateTime).inMinutes - (now.difference(dateTime).inMinutes / 70).floor() * 70);
      if (remain > 60 && remain < 70) {
        isOnBreak = true;
      } else {
        isOnBreak = false;
        remainGroupTime = remain;
      }
    }
    channelName = 'room${room.id}';
    currentUsers = [];
    currentUsers.addAll(room.listUser ?? []);
    allUsers = room.listUser ?? [];
    debugPrint('Data: ${jsonEncode(room.toJson())}');
    var userData = SharedPrefs.getValue('userData');
    debugPrint(userData);
    roomStream.studyController.sink.add(allUsers);

    try {
      userModel = UserModel.fromJson(jsonDecode(userData));
      // remove yourself in list
      currentUsers.removeWhere((element) => element.id == userModel.uuid);
    } catch (e) {}
    notifyListeners();
  }

  void initTimeController() {
    endGroupTime =
        DateTime.now().millisecondsSinceEpoch + 1000 * 60 * (groupMinutes - remainGroupTime);

    groupTimeController =
        CountdownTimerController(endTime: endGroupTime, onEnd: onBreakTime);

    // var endPersonTime =
    //     DateTime.now().millisecondsSinceEpoch + 1000 * 60 * personalMinutes;
    // personalTimeController =
    //     CountdownTimerController(endTime: endPersonTime, onEnd: () {});

    endBreakTime =
        DateTime.now().millisecondsSinceEpoch + 1000 * 60 * (breakMinutes - remainBreakTime);
    breakTimeController =
        CountdownTimerController(endTime: endBreakTime, onEnd: onOverBreakTime);

    timeController = isOnBreak ? breakTimeController : groupTimeController;
  }

  void showOption() {
    isShowOption = !isShowOption;
    notifyListeners();
  }

  Future<void> initEngine() async {
    engine = await RtcEngine.createWithContext(
        RtcEngineContext(AppConstant.agoraKey));
    _addListeners();

    await engine.enableVideo();
    await engine.startPreview();
    await engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
    await engine.setClientRole(ClientRole.Broadcaster);

    _joinChannel();
  }

  void _addListeners() {
    engine.setEventHandler(RtcEngineEventHandler(
      warning: (warningCode) {},
      error: (errorCode) {},
      joinChannelSuccess: (channel, uid, elapsed) {
        isJoined = true;
        notifyListeners();
      },
      userJoined: (uid, elapsed) {
        remoteUid.add(uid);
        notifyListeners();
      },
      userOffline: (uid, reason) {
        remoteUid.removeWhere((element) => element == uid);
        notifyListeners();
      },
      leaveChannel: (stats) {
        isJoined = false;
        remoteUid.clear();
        notifyListeners();
      },
    ));
  }

  void listenSocketEvents() async {
    // join chat
    socketService?.emit(SocketEvent.joinChannel, {
      'data': {'roomId': room.id, 'channelId': room.id, 'type': 'meeting'}
    });

    socketService?.on(SocketEvent.userJoined, (data) {
      var dataValue = jsonDecode(data)['data'];
      List<StudyDetail> temp = (dataValue['listUser'] as List<dynamic>?)
              ?.map((e) => StudyDetail.fromJson(e as Map<String, dynamic>))
              .toList() ??
          [];
      if (temp.isNotEmpty) {
        allUsers = temp;
        room.listUser = allUsers;
        roomStream.studyController.sink.add(allUsers);
        temp.removeWhere((element) => element.id == userModel.uuid);
        currentUsers = temp;
        notifyListeners();
      }
    });

    socketService?.on(SocketEvent.userLeft, (data) {
      debugPrint('USER LEFT: ${jsonDecode(data)}');
      var dataValue = jsonDecode(data)['data'];
      List<StudyDetail> temp = (dataValue['listUser'] as List<dynamic>?)
              ?.map((e) => StudyDetail.fromJson(e as Map<String, dynamic>))
              .toList() ??
          [];
      if (temp.isNotEmpty) {
        allUsers = temp;
        room.listUser = allUsers;
        roomStream.studyController.sink.add(allUsers);
        temp.removeWhere((element) => element.id == userModel.uuid);
        currentUsers = temp;
        notifyListeners();
      }
    });

    socketService?.on(SocketEvent.userChangeSetting, (data) {
      debugPrint('USER Change settings: ${jsonDecode(data)}');
      // todo update list current user with new setting
      var dataValue = jsonDecode(data);
      if (dataValue['userId']) {
        currentUsers = currentUsers.map((e) {
          if (e.id == (dataValue['userId'] as int)) {
            e.studySession.isCamera = dataValue['settings']['isCamera'];
            e.studySession.isMicro = dataValue['settings']['isMicro'];
          }
          return e;
        }).toList();
      }

      notifyListeners();
    });

    socketService?.on(SocketEvent.userSentMessage, (data) {
      var messageJson = jsonDecode(data)['data'];
      var message = Message.fromJson(messageJson);
      debugPrint("Message ${jsonEncode(messageJson)}");
      if (message.user?.uuid == userModel.uuid) {
        message.isSender = true;
      } else {
        message.isSender = false;
      }
      messages.add(message);
      messageStream.messageController.sink.add(messages);
      notifyListeners();
    });
  }

  void onBreakTime() async {
    socketService?.emit(SocketEvent.breakRoom, {
      'data': {
        'roomId': room.id,
        'settings': {'isCamera': setting.isCamera, 'isMicro': setting.isMicro}
      }
    });
    isOnBreak = true;
    endBreakTime =
        DateTime.now().millisecondsSinceEpoch + 1000 * 60 * breakMinutes;
    timeController =
        CountdownTimerController(endTime: endBreakTime, onEnd: onOverBreakTime);
    notifyListeners();
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: Container(
                color: Colors.white,
                height: 100,
                child: const Center(
                  child: Text(
                      'Congratulation! You had studied for a long time. Let`s break some minutes!'),
                ),
              ),
            ));
  }

  void onOverBreakTime() async {
    socketService?.emit(SocketEvent.overBreakRoom, {
      'data': {
        'roomId': room.id,
        'settings': {'isCamera': setting.isCamera, 'isMicro': setting.isMicro}
      }
    });
    isOnBreak = false;
    endGroupTime =
        DateTime.now().millisecondsSinceEpoch + 1000 * 60 * groupMinutes;
    timeController =
        CountdownTimerController(endTime: endGroupTime, onEnd: onBreakTime);
    notifyListeners();
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: Container(
                color: Colors.white,
                height: 100,
                child: const Center(
                  child: Text('Let`s continue!'),
                ),
              ),
            ));
  }

  void getMessage() async {
    try {
      var res = await chatService.getChannelChatByName('chat_room${room.id}');
      messages = res.data.messages ?? [];
    } on DioError catch (e) {
      debugPrint(e.message);
    }
    notifyListeners();
  }

  void getGoals() async {
    try {
      var res = await goalService.getAllGoals();
      goals = res.data;
    } on DioError catch (e) {
      debugPrint(e.message);
    }
    notifyListeners();
  }

  void _joinChannel() async {
    if (defaultTargetPlatform == TargetPlatform.android) {
      await [Permission.microphone, Permission.camera].request();
    }

    await engine.joinChannel(token, channelName, null, userModel.uuid ?? 0);
  }

  Future<void> leftRoom() async {
    socketService!.emit(SocketEvent.leftRoom, {
      'data': {'roomId': room.id}
    });
  }

  void setChannel(channel) {
    channelName = channel;
    notifyListeners();
  }

  Future leaveChannel() async {
    await engine.leaveChannel();
    await leftRoom();
    try {
      engine.destroy();
    } catch (e) {
      debugPrint("Destroy engine error");
    }
  }

  void switchCamera() {
    engine.switchCamera().then((value) {
      isSwitchCamera = !isSwitchCamera;
    }).catchError((err) {});
    notifyListeners();
  }

  void switchRender() {
    isSwitchRender = !isSwitchRender;
    remoteUid = List.of(remoteUid.reversed);
    notifyListeners();
  }

  // handle volume
  void mute() {
    hasVolume = !hasVolume;
    engine.muteAllRemoteAudioStreams(hasVolume);
    notifyListeners();
  }

  void handleVisibleCamera() {
    hasCamera = !hasCamera;
    engine.muteLocalVideoStream(hasCamera);
    notifyListeners();
  }

  // handle local mic
  void handleSpeech() async{
    hasAudio = !hasAudio;
    await engine.muteLocalAudioStream(hasAudio);
    notifyListeners();
  }

  void turnOnCamera() async {
    setting.isCamera = !setting.isCamera;
    await engine.muteLocalVideoStream(setting.isCamera);
    socketService!.emit(SocketEvent.changeSetting, {
      'data': {
        'roomId': room.id,
        'settings': {
          'isCamera': setting.isCamera,
          'isMicro': setting.isMicro
        }
      }
    });
    notifyListeners();
  }

  void turnOnMicro() async{
    setting.isMicro = !setting.isMicro;
    await engine.muteLocalAudioStream(setting.isMicro);
    socketService!.emit(SocketEvent.changeSetting, {
      'data': {
        'roomId': room.id,
        'settings': {
          'isCamera': setting.isCamera,
          'isMicro': setting.isMicro
        }
      }
    });
    notifyListeners();
  }

  @override
  void onAppear() {
    // TODO: implement onAppear
  }

  @override
  void onDisAppear() {
    // TODO: implement onDisAppear
  }

  @override
  void dispose() {
    socketService?.clearListeners();
    super.dispose();
  }
}
