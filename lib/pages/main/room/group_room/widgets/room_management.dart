import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:studywithme/base/base_page.dart';
import 'package:studywithme/pages/main/room/group_room/group_room_vm.dart';
import 'package:studywithme/pages/main/room/group_room/widgets/room_management_vm.dart';

import '../../../../../models/message.dart';
import '../../../../../models/room.dart';
import '../../../chats/room_chat/room_chat.dart';

class RoomManagement extends StatefulWidget {
  final List<Message> messages;
  final Room room;
  final int initIndex;
  const RoomManagement(
      {Key? key,
      required this.room,
      required this.messages,
      required this.initIndex})
      : super(key: key);

  @override
  _RoomManagementState createState() => _RoomManagementState();
}

class _RoomManagementState extends State<RoomManagement>
    with MixinBasePage<RoomManagementVM> {
  @override
  Widget build(BuildContext context) {
    final providerGroup = Provider.of<GroupRoomVM>(context);
    return builder(() => Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: DefaultTabController(
            length: 2,
            initialIndex: widget.initIndex,
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height * 0.6),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const TabBar(
                    labelColor: Colors.red,
                    tabs: <Widget>[
                      Tab(
                        text: 'Member',
                      ),
                      Tab(
                        text: 'Chat',
                      )
                    ],
                  ),
                  Expanded(
                    //I want to use dynamic height instead of fixed height
                    child: TabBarView(
                      children: <Widget>[
                        SizedBox(
                          height: 400,
                          child: SingleChildScrollView(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                const SizedBox(
                                  height: 10,
                                ),
                                const Text('2 students in room'),
                                const SizedBox(
                                  height: 15,
                                ),
                                SizedBox(
                                  height: 300,
                                  child: ListView.builder(
                                      itemCount: 2,
                                      itemBuilder: (context, index) => Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 8),
                                            child: SizedBox(
                                              height: 20,
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                children: [
                                                  Text(index == 0
                                                      ? 'ducmh1@gmail.com'
                                                      : 'ducmh2@gmail.com'),
                                                  const Spacer(),
                                                  InkWell(
                                                      onTap: () {},
                                                      child: const Icon(
                                                          Icons.more_horiz))
                                                ],
                                              ),
                                            ),
                                          )),
                                )
                              ],
                            ),
                          ),
                        ),
                        RoomChat(
                          room: provider.room,
                          messages: provider.messages,
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }

  @override
  RoomManagementVM create() => RoomManagementVM();

  @override
  void initialise(BuildContext context) {
    debugPrint('Data: ${jsonEncode(widget.room.toJson())}');
    provider.initRoom(widget.room, widget.messages);
  }
}
