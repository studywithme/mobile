import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:studywithme/base/base_viewmodel.dart';
import 'package:studywithme/models/message.dart';
import '../../../../../base/di/locator.dart';
import '../../../../../models/room.dart';
import '../../../../../models/stream/chat_stream.dart';
import '../../../../../models/stream/room_stream.dart';
import '../../../../../models/study_detail.dart';
import '../../../../../services/remote/socket_client.dart';

class RoomManagementVM extends BaseViewModel {
  late Room room;
  late List<Message> messages = [];
  final socketService = locator<StreamSocket>().socket;
  final RoomStream roomStream = locator<RoomStream>();
  final MessageStream messageStream = locator<MessageStream>();
  late StreamSubscription subscriptionRoom;
  late StreamSubscription subscriptionMessage;
  late List<StudyDetail> currentUsers = [];
  // late List<dynamic> currents = []
  @override
  void onAppear() {}

  @override
  void onDisAppear() {}

  @override
  void onInit() {
    // listen
    listenStream();
  }

  void initRoom(Room room, List<Message> messages) {
    this.room = room;
    currentUsers = room.listUser ?? [];
    this.messages = messages;
    debugPrint('Data: ${jsonEncode(room.toJson())}');

    notifyListeners();
  }

  void listenStream() {
    subscriptionRoom = roomStream.studyStream.listen((event) {
      currentUsers = event;
      notifyListeners();
    });
    subscriptionMessage = messageStream.messageStream.listen((event) {
      debugPrint("OK");
      messages = event;
      notifyListeners();
    });
  }

  @override
  void dispose() {
    debugPrint("DISPOSE: Room Management");
    subscriptionRoom.cancel();
    subscriptionMessage.cancel();
    super.dispose();
  }
}
