import 'package:flutter/material.dart';
import 'package:studywithme/base/base_page.dart';
import 'package:studywithme/pages/main/room/group_room/group_room.dart';
import 'package:studywithme/pages/main/room/lobby_page/lobby_vm.dart';

import '../../../../components/room_button.dart';
import '../../../../models/room.dart';

class LobbyPage extends StatefulWidget {
  final Room room;
  const LobbyPage({Key? key, required this.room}) : super(key: key);

  @override
  _LobbyPageState createState() => _LobbyPageState();
}

class _LobbyPageState extends State<LobbyPage> with MixinBasePage<LobbyVM> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return builder(() => WillPopScope(
          onWillPop: () async => false,
          child: Scaffold(
            body: Stack(
              alignment: Alignment.center,
              children: [
                Positioned.fill(
                    child: Container(
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              'https://i.scdn.co/image/ab67616d0000b2735668c6562f6ebc56d2c44d06'))),
                  child: Center(
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 15),
                      height: 130,
                      color: Colors.black.withOpacity(0.8),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          // SizedBox(
                          //   height: 40,
                          //   width: MediaQuery.of(context).size.width * 0.6,
                          //   child: Row(
                          //     mainAxisAlignment: MainAxisAlignment.center,
                          //     crossAxisAlignment: CrossAxisAlignment.center,
                          //     children: [
                          //       const Icon(
                          //         Icons.mic,
                          //         color: Colors.white,
                          //       ),
                          //       Switch(
                          //           value: provider.setting.isMicro,
                          //           onChanged: provider.turnOnMicro),
                          //       const SizedBox(
                          //         width: 10,
                          //       ),
                          //       const Icon(
                          //         Icons.videocam_rounded,
                          //         color: Colors.white,
                          //       ),
                          //       Switch(
                          //           value: provider.setting.isCamera,
                          //           onChanged: provider.turnOnCamera),
                          //     ],
                          //   ),
                          // ),
                          // const SizedBox(
                          //   height: 10,
                          // ),
                          ElevatedButton(
                            onPressed: () {
                              provider.joinRoom(context);
                            },
                            child: const Text(
                              'Join',
                              style: TextStyle(
                                  color: Colors.white,
                                  decoration: TextDecoration.underline),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )),
                // Positioned(
                //     top: 30,
                //     right: 20,
                //     child: RoomButton(
                //       icon: Icon(
                //         provider.isShowOption ? Icons.close : Icons.more_vert,
                //         color: Colors.white,
                //       ),
                //       onTap: provider.showOption,
                //     )),
                // Positioned(
                //   top: 95,
                //   right: 20,
                //   child: Visibility(
                //     visible: provider.isShowOption,
                //     child: SizedBox(
                //       height: 200,
                //       width: 55,
                //       child: ListView(
                //         children: [
                //           RoomButton(
                //             icon: const Icon(
                //               Icons.photo,
                //               color: Colors.white,
                //             ),
                //             onTap: () {},
                //           ),
                //           const SizedBox(
                //             height: 10,
                //           ),
                //           RoomButton(
                //             icon: const Icon(
                //               Icons.music_note,
                //               color: Colors.white,
                //             ),
                //             onTap: () {},
                //           )
                //         ],
                //       ),
                //     ),
                //   ),
                // ),
                Positioned(
                    bottom: 20,
                    right: 20,
                    child: RoomButton(
                      icon: const Icon(
                        Icons.exit_to_app,
                        color: Colors.red,
                      ),
                      onTap: () {
                        Navigator.pop(context);
                      },
                    ))
              ],
            ),
          ),
        ));
  }

  @override
  LobbyVM create() => LobbyVM();

  @override
  void initialise(BuildContext context) {
    provider.initContext(context, mounted);
    provider.initRoom(widget.room);
  }
}
