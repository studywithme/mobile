import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:studywithme/base/base_viewmodel.dart';
import 'package:studywithme/constants/socket_event.dart';
import 'package:studywithme/models/room_option.dart';
import 'package:studywithme/pages/main/room/group_room/group_room.dart';
import 'package:studywithme/pages/main/room/group_room/group_room_vm.dart';
import 'package:studywithme/services/remote/api_client.dart';
import 'package:studywithme/services/remote/socket_client.dart';

import '../../../../base/di/locator.dart';
import '../../../../models/room.dart';

class LobbyVM extends BaseViewModel {
  late Room room;
  late bool mounted = false;
  late bool isShowOption = false;
  late RoomOption setting = RoomOption();

  @override
  void onInit() {}

  void initContext(BuildContext context, bool mounted) {
    this.mounted = mounted;
  }

  void initRoom(Room initRoom) {
    room = initRoom;
    notifyListeners();
  }

  void showOption() {
    isShowOption = !isShowOption;
    notifyListeners();
  }

  void joinRoom(context) async {
    final socketService = locator<StreamSocket>();
    final roomService = locator<ApiClient>().roomService;
    String rtcToken = '';
    showLoading();
    await roomService.getRoomRtcToken(room.id).then((value) {
      rtcToken = value['token'] ?? '';
      debugPrint('RTC Token: $rtcToken');
      socketService.emitEvent(SocketEvent.joinRoom, {
        'data': {
          'roomId': room.id,
          'settings': {'isCamera': setting.isCamera, 'isMicro': setting.isMicro}
        }
      });
    }).catchError((err) async {
      showError("Can not join this room right nows!");
      await Future.delayed(const Duration(seconds: 2));
      if (!mounted) {
        return;
      }
      Navigator.pop(context);
    });
    socketService.socket?.on(SocketEvent.joinRoomSuccess, (data) {
      hideLoading();
      if (!mounted) {
        return;
      }
      debugPrint('Data $data');
      var room = Room.fromJson(jsonDecode(data)['data']);
      debugPrint('context $context');
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => GroupRoomVM(),
                child: GroupRoomPage(
                  token: rtcToken,
                  room: room,
                  setting: setting,
                )),
          ));
    });

    socketService.socket?.on(SocketEvent.joinRoomFail, (data) async {
      showError('Can not join this room right now!');
      await Future.delayed(const Duration(seconds: 2));
      if (!mounted) {
        return;
      }
      Navigator.pop(context);
    });
  }

  void turnOnCamera(bool value) {
    setting.isCamera = value;
    notifyListeners();
  }

  void turnOnMicro(bool value) {
    setting.isMicro = value;
    notifyListeners();
  }

  @override
  void onAppear() {
    // TODO: implement onAppear
  }

  @override
  void onDisAppear() {
    // TODO: implement onDisAppear
  }
}
