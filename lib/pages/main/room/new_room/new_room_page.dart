import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:studywithme/base/base_page.dart';
import 'package:studywithme/components/custom_button.dart';
import 'package:studywithme/pages/main/room/new_room/new_room_vm.dart';

import '../../../../components/custom_numer_chosen.dart';

class NewRoomPage extends StatefulWidget {
  const NewRoomPage({Key? key}) : super(key: key);

  @override
  _NewRoomPageState createState() => _NewRoomPageState();
}

class _NewRoomPageState extends State<NewRoomPage>
    with MixinBasePage<NewRoomVM> {
  @override
  Widget build(BuildContext context) {
    return builder(() => SingleChildScrollView(
          child: SizedBox(
            height: 450,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                const Text(
                  'Create your room',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 280,
                  child: ListView(
                    children: [
                      const Text('Room name *',
                          style: TextStyle(fontWeight: FontWeight.w400)),
                      SizedBox(
                        height: 50,
                        child: TextFormField(
                          controller: provider.roomName,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.deepPurpleAccent
                                          .withOpacity(0.2),
                                      width: 0.01)),
                              focusedBorder: const OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.deepPurpleAccent))),
                        ),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      const Text(
                        'Number of participants *',
                        style: TextStyle(fontWeight: FontWeight.w400),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      CustomNumberSelected(
                          minValue: provider.minValue,
                          maxValue: provider.maxValue,
                          value: provider.roomCount,
                          onAdd: provider.onIncreaseCount,
                          onMinus: provider.onDecreaseCount),
                      const SizedBox(
                        height: 8,
                      ),
                      const Text('Room description(optional)',
                          style: TextStyle(fontWeight: FontWeight.w400)),
                      const SizedBox(
                        height: 5,
                      ),
                      TextFormField(
                        maxLines: 3,
                        controller: provider.roomDescription,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.deepPurpleAccent
                                        .withOpacity(0.2),
                                    width: 0.01)),
                            focusedBorder: const OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.deepPurpleAccent))),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 5),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Icon(
                      Icons.help_outline,
                      color: Colors.deepPurpleAccent,
                    ),
                    Expanded(
                      child: Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(text: 'The room will '),
                            TextSpan(
                              text: 'automatic close',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            TextSpan(
                                text:
                                    ' when the last participant leaves the room'),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                const Spacer(),
                CustomButton(
                  title: 'Create',
                  onPress: provider.createRoom,
                  color: Colors.deepOrangeAccent,
                )
              ],
            ),
          ),
        ));
  }

  @override
  NewRoomVM create() => NewRoomVM();

  @override
  void initialise(BuildContext context) {
    provider.initContext(context, mounted);
  }
}
