import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:studywithme/base/base_viewmodel.dart';
import 'package:studywithme/base/di/locator.dart';
import 'package:studywithme/models/room.dart';
import 'package:studywithme/pages/main/room/lobby_page/lobby_page.dart';
import 'package:studywithme/services/remote/api_client.dart';

import '../../../../services/remote/bodies/room_body.dart';

class NewRoomVM extends BaseViewModel {
  final room = RoomBody();
  final roomName = TextEditingController();
  late int roomCount = 5;
  final roomDescription = TextEditingController();

  final int minValue = 2;
  final int maxValue = 10;

  late BuildContext context;
  late bool mounted;

  @override
  void onInit() {}

  void initContext(BuildContext context, bool mounted) {
    this.context = context;
    this.mounted = mounted;
  }

  void createRoom() async {
    final roomService = locator<ApiClient>().roomService;
    room.type = RoomType.group;
    room.name = roomName.text;
    room.limit = roomCount;
    room.description = roomDescription.text;
    showLoading();
    try {
      final res = await roomService.createNewRoom(room);
      //showSuccess('Create room success!');
      // join to new room
      hideLoading();
      if (!mounted) {
        return;
      }
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => LobbyPage(
                    room: res,
                  )));
    } on DioError catch (e) {
      showError(e.message);
    }
  }

  void onIncreaseCount() {
    if (roomCount < maxValue) {
      roomCount++;
      notifyListeners();
    }
  }

  void onDecreaseCount() {
    if (roomCount > minValue) {
      roomCount--;
      notifyListeners();
    }
  }

  @override
  void onAppear() {
    // TODO: implement onAppear
  }

  @override
  void onDisAppear() {
    // TODO: implement onDisAppear
  }
}
