import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:provider/provider.dart';
import 'package:studywithme/base/base_page.dart';
import 'package:studywithme/components/custom_input.dart';
import 'package:studywithme/components/custom_numer_chosen.dart';
import 'package:studywithme/components/skeleton/group_item_skeleton.dart';
import 'package:studywithme/pages/main/room/new_room/new_room_page.dart';
import 'package:studywithme/pages/main/room/room_vm.dart';
import 'package:studywithme/pages/main/room/widgets/group_room_item.dart';
import 'package:studywithme/pages/main/room/widgets/solo_room_widget.dart';
import 'package:studywithme/utils/colors.dart';

import '../../../generated/l10n.dart';
import '../../../models/room.dart';

class RoomPage extends StatefulWidget {
  const RoomPage({Key? key}) : super(key: key);

  @override
  _RoomPageState createState() => _RoomPageState();
}

class _RoomPageState extends State<RoomPage> with MixinBasePage<RoomVM> {
  @override
  Widget build(BuildContext context) {
    return builder(() => DefaultTabController(
          length: 3,
          child: Scaffold(
              backgroundColor: AppColor.hf4f2f2,
              appBar: AppBar(
                elevation: 0.0,
                backgroundColor: AppColor.hf4f2f2,
                leading: Container(
                  height: 30,
                  width: 30,
                  margin: const EdgeInsets.only(left: 10, top: 5, bottom: 5),
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              'https://lh3.googleusercontent.com/a-/ACNPEu8jektNVZI_RUWvmFu7cXhsGDsF7cmkGw3-G0ud=s96-c'))),
                ),
                title: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text(
                      "Good Morning 👋",
                      style: TextStyle(fontSize: 15, color: Colors.black45),
                    ),
                    Text(
                      "Duc",
                      style: TextStyle(fontSize: 16, color: Colors.black),
                    )
                  ],
                ),
                actions: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: IconButton(
                        onPressed: showDialogAddRoom,
                        icon: const Icon(
                          Icons.add,
                          color: Colors.blueAccent,
                        )),
                  ),
                ],
                bottom: PreferredSize(
                  preferredSize: const Size.fromHeight(40),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Stack(
                      fit: StackFit.passthrough,
                      alignment: Alignment.bottomCenter,
                      children: <Widget>[
                        Container(
                          decoration: const BoxDecoration(
                            border: Border(
                              bottom:
                                  BorderSide(color: Colors.grey, width: 2.0),
                            ),
                          ),
                        ),
                        TabBar(
                            indicatorColor: Colors.deepPurpleAccent,
                            unselectedLabelColor: Colors.grey,
                            labelColor: Colors.black,
                            onTap: (index) {},
                            tabs: const [
                              Tab(
                                  child: Text(
                                'Live Rooms',
                              )),
                              Tab(
                                  child: Text(
                                'Favourites',
                              )),
                              Tab(
                                  child: Text(
                                'My Rooms',
                              )),
                            ]),
                      ],
                    ),
                  ),
                ),
              ),
              body: TabBarView(children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 120,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                'Solo Study Room',
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              SoloRoomItem(roomInfo: Room()),
                            ],
                          ),
                        ),
                        const Text(
                          'Study Together Rooms',
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          height: 300,
                          child: ListView.builder(
                              itemCount: provider.isLoading
                                  ? 4
                                  : provider.listGroupRoom.length,
                              itemBuilder: (context, index) =>
                                  provider.isLoading
                                      ? const GroupItemSkeleton()
                                      : GroupRoomItem(
                                          onActionFavourite: provider
                                                  .listGroupRoom[index]
                                                  .isFavourite
                                              ? provider.removeFavourite
                                              : provider.addFavourite,
                                          room: provider.listGroupRoom[index],
                                        )),
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
                  child: SizedBox(
                    height: 500,
                    child: ListView.builder(
                        itemCount: provider.loadFavourite
                            ? 4
                            : provider.favourites.length,
                        itemBuilder: (context, index) => provider.loadFavourite
                            ? const GroupItemSkeleton()
                            : GroupRoomItem(
                                onActionFavourite:
                                    provider.favourites[index].isFavourite
                                        ? provider.removeFavourite
                                        : provider.addFavourite,
                                room: provider.favourites[index],
                              )),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
                  child: SizedBox(
                    height: 500,
                    child: ListView.builder(
                        itemCount:
                            provider.loadMyRoom ? 4 : provider.myRooms.length,
                        itemBuilder: (context, index) => provider.loadMyRoom
                            ? const GroupItemSkeleton()
                            : GroupRoomItem(
                                onActionFavourite:
                                    provider.myRooms[index].isFavourite
                                        ? provider.removeFavourite
                                        : provider.addFavourite,
                                room: provider.myRooms[index],
                              )),
                  ),
                ),
              ])),
        ));
  }

  void showDialogAddRoom() async {
    var valueProvider = Provider.of<RoomVM>(context, listen: false);
    showDialog(
        context: context,
        builder: (context) => ChangeNotifierProvider.value(
              value: valueProvider,
              child: const AlertDialog(
                content: NewRoomPage(),
              ),
            ));
  }

  @override
  RoomVM create() => RoomVM();

  @override
  void initialise(BuildContext context) {}
}
