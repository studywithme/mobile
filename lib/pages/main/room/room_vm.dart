import 'package:dio/dio.dart';
import 'package:studywithme/base/base_viewmodel.dart';
import 'package:studywithme/base/di/locator.dart';
import 'package:studywithme/services/remote/api_client.dart';

import '../../../models/room.dart';

class RoomVM extends BaseViewModel {
  final roomService = locator<ApiClient>().roomService;
  final favouriteService = locator<ApiClient>().favouriteService;
  List<Room> listGroupRoom = [];
  List<Room> favourites = [
    // Room.fromJson({
    //   'id': 1,
    //   'name': 'Learn English',
    //   'maxCount': 15,
    //   'isFavourite': true,
    //   'status': 'inactive',
    //   'type': 'group',
    //   'description': 'Let`s study together'
    // }),
    // Room.fromJson({
    //   'id': 1,
    //   'name': 'Let`s go',
    //   'maxCount': 5,
    //   'isFavourite': true,
    //   'status': 'inactive',
    //   'type': 'group'
    // })
  ];
  List<Room> myRooms = [
    // Room.fromJson({
    //   'id': 2,
    //   'name': 'Learn Math',
    //   'maxCount': 10,
    //   'isFavourite': false,
    //   'status': 'inactive',
    //   'type': 'group'
    // })
  ];
  bool loadFavourite = false;
  bool loadMyRoom = false;
  @override
  void onInit() {
    getAllRoom();
    getFavourites();
    getMyRooms();
  }

  void getSingleRoomData() async {}

  void getAllRoom() async {
    isLoading = true;
    try {
      final data = await roomService.getRooms();
      listGroupRoom.addAll(data.data);
      listGroupRoom = listGroupRoom
          .where((element) =>
              element.currentUsers != null && element.currentUsers! > 0)
          .toList();
    } on DioError catch (e) {
      showError(e.message);
    }
    isLoading = false;
    notifyListeners();
  }

  void getMyRooms() async {
    loadMyRoom = true;
    myRooms = [];
    try {
      final data = await roomService.getMyRooms();
      myRooms.addAll(data.data);
      //myRooms = myRooms.where((element) => element.isAuthor ?? false).toList();
    } on DioError catch (e) {
      showError(e.message);
    }
    loadMyRoom = false;
    notifyListeners();
  }

  void getFavourites() async {
    loadFavourite = true;
    favourites = [];
    try {
      final data = await roomService.getFavouriteRooms();
      favourites.addAll(data.data);
      favourites = favourites.where((element) => element.isFavourite).toList();
    } on DioError catch (e) {
      showError(e.message);
    }
    loadFavourite = false;
    notifyListeners();
  }

  void addFavourite(room) async {
    try {
      final res = await favouriteService.addFavourite({'roomId': room.id});
      if (res.result) {
        favourites.add(room);
        myRooms = myRooms.map((element) {
          if (element.id == room.id) {
            element.isFavourite = true;
          }
          return element;
        }).toList();
        listGroupRoom = listGroupRoom.map((element) {
          if (element.id == room.id) {
            element.isFavourite = true;
          }
          return element;
        }).toList();
      }
      notifyListeners();
    } catch (e) {}
  }

  void removeFavourite(room) async {
    try {
      final res = await favouriteService.removeFavourite({'roomId': room.id});
      if (res.result) {
        favourites =
            favourites.where((element) => element.id != room.id).toList();
        myRooms = myRooms.map((element) {
          if (element.id == room.id) {
            element.isFavourite = false;
          }
          return element;
        }).toList();
        listGroupRoom = listGroupRoom.map((element) {
          if (element.id == room.id) {
            element.isFavourite = false;
          }
          return element;
        }).toList();
      }
      notifyListeners();
    } catch (e) {}
  }

  @override
  void onAppear() {
    // TODO: implement onAppear
  }

  @override
  void onDisAppear() {
    // TODO: implement onDisAppear
  }
}
