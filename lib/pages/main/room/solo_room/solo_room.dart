import 'package:flutter/material.dart';
import 'package:studywithme/base/base_page.dart';
import 'package:studywithme/components/goal_status.dart';
import 'package:studywithme/components/room_button.dart';
import 'package:studywithme/components/time_counter.dart';
import 'package:studywithme/pages/main/room/solo_room/solo_room_vm.dart';
import 'package:studywithme/pages/main/room/widgets/session_goal.dart';

import '../../app_page.dart';

class SoloRoom extends StatefulWidget {
  const SoloRoom({Key? key}) : super(key: key);

  @override
  _SoloRoomState createState() => _SoloRoomState();
}

class _SoloRoomState extends State<SoloRoom> with MixinBasePage<SoloRoomVM> {
  @override
  Widget build(BuildContext context) {
    return builder(() => WillPopScope(
          onWillPop: () async => false,
          child: Scaffold(
            body: Stack(
              children: [
                Positioned.fill(
                    child: Container(
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: NetworkImage(
                              'https://i.scdn.co/image/ab67616d0000b2735668c6562f6ebc56d2c44d06'))),
                )),
                Positioned(
                    top: 20,
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 5),
                      height: 120,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              TimeCounter(
                                isOnBreak: provider.isOnBreak,
                                controller: provider.timeController,
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          InkWell(
                              onTap: () {
                                showModalBottomSheet(
                                  isScrollControlled: true,
                                  context: context,
                                  backgroundColor: Colors.white,
                                  shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadiusDirectional.only(
                                      topEnd: Radius.circular(25),
                                      topStart: Radius.circular(25),
                                    ),
                                  ),
                                  builder: (context) => SingleChildScrollView(
                                    padding: EdgeInsetsDirectional.only(
                                      start: 20,
                                      end: 20,
                                      bottom: 30,
                                      top: 8,
                                    ),
                                    child: SessionGoalWidget(
                                      goals: provider.goals,
                                    ),
                                  ),
                                );
                              },
                              child: GoalStatus(
                                goals: provider.goals,
                              ))
                        ],
                      ),
                    )),
                // Positioned(
                //     top: 30,
                //     right: 20,
                //     child: RoomButton(
                //       icon: Icon(
                //         provider.isShowOption ? Icons.close : Icons.more_vert,
                //         color: Colors.white,
                //       ),
                //       onTap: provider.showOption,
                //     )),
                // Positioned(
                //   top: 95,
                //   right: 20,
                //   child: Visibility(
                //     visible: provider.isShowOption,
                //     child: SizedBox(
                //       height: 200,
                //       width: 55,
                //       child: ListView(
                //         children: [
                //           RoomButton(
                //             icon: const Icon(
                //               Icons.photo,
                //               color: Colors.white,
                //             ),
                //             onTap: () {},
                //           ),
                //           const SizedBox(
                //             height: 10,
                //           ),
                //           RoomButton(
                //             icon: const Icon(
                //               Icons.music_note,
                //               color: Colors.white,
                //             ),
                //             onTap: () {},
                //           )
                //         ],
                //       ),
                //     ),
                //   ),
                // ),
                Positioned(
                    bottom: 20,
                    right: 20,
                    child: RoomButton(
                      icon: const Icon(
                        Icons.exit_to_app,
                        color: Colors.red,
                      ),
                      onTap: () {
                        showConfirmLeaveRoom();
                        //Navigator.pop(context);
                      },
                    ))
              ],
            ),
          ),
        ));
  }

  showConfirmLeaveRoom() {
    showDialog(
        context: context,
        builder: (dialogContext) => AlertDialog(
              content: const Text(
                  'Are you sure you want to end your study session?'),
              actions: [
                TextButton(
                    onPressed: () async {
                      await provider.leftRoom();
                      if (!mounted) return;
                      provider.dispose();
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const AppPage()));
                    },
                    child: const Text('Leave call')),
                TextButton(
                    onPressed: () {
                      Navigator.pop(dialogContext);
                    },
                    child: const Text('Stay in session'))
              ],
            ));
  }

  @override
  SoloRoomVM create() => SoloRoomVM();

  @override
  void initialise(BuildContext context) {
    provider.initContext(context, mounted);
  }

  @override
  void dispose() {
    provider.timeController.dispose();
    super.dispose();
  }
}
