import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/countdown_timer_controller.dart';

import '../../../../base/base_viewmodel.dart';
import '../../../../base/di/locator.dart';
import '../../../../constants/socket_event.dart';
import '../../../../models/goal.dart';
import '../../../../services/local/shared_prefs.dart';
import '../../../../services/remote/api_client.dart';
import '../../../../services/remote/socket_client.dart';

class SoloRoomVM extends BaseViewModel {
  bool isShowOption = false;
  final goalService = locator<ApiClient>().goalService;
  late CountdownTimerController timeController;
  final socketService = locator<StreamSocket>().socket;
  List<Goal> goals = [];

  int personalMinutes = 1;
  int breakMinutes = 1;

  late int endPersonTime;
  late int endBreakTime;

  late BuildContext context;
  late bool mounted = false;

  bool isOnBreak = false;
  bool isPersonalTime = false;

  SoloRoomVM() {
    initTime();
    joinRoom();
    getGoals();
    initTimeController();
  }

  @override
  void onInit() {}

  void initContext(BuildContext context, bool mounted) {
    this.context = context;
    this.mounted = mounted;
  }

  void initTime() {
    var data = SharedPrefs.getValue('studyTime');
    personalMinutes = int.tryParse(data.isNotEmpty ? data : '60')!;
    notifyListeners();
  }

  void initTimeController() {
    endPersonTime =
        DateTime.now().millisecondsSinceEpoch + 1000 * 60 * personalMinutes;
    endBreakTime =
        DateTime.now().millisecondsSinceEpoch + 1000 * 60 * breakMinutes;
    timeController =
        CountdownTimerController(endTime: endPersonTime, onEnd: onBreakTime);
  }

  void onBreakTime() async {
    socketService?.emit(SocketEvent.breakRoom, {
      'data': {
        'roomId': null,
        'settings': {'isCamera': false, 'isMicro': false}
      }
    });
    isOnBreak = true;
    endBreakTime =
        DateTime.now().millisecondsSinceEpoch + 1000 * 60 * breakMinutes;
    timeController =
        CountdownTimerController(endTime: endBreakTime, onEnd: onOverBreakTime);
    notifyListeners();
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: Container(
                color: Colors.white,
                height: 100,
                child: const Center(
                  child: Text(
                      'Congratulation! You had studied for a long time. Let`s break some minutes!'),
                ),
              ),
            ));
  }

  void onOverBreakTime() async {
    socketService?.emit(SocketEvent.overBreakRoom, {
      'data': {
        'roomId': null,
        'settings': {'isCamera': false, 'isMicro': false}
      }
    });
    isOnBreak = false;
    endPersonTime =
        DateTime.now().millisecondsSinceEpoch + 1000 * 60 * personalMinutes;
    timeController =
        CountdownTimerController(endTime: endPersonTime, onEnd: onBreakTime);
    notifyListeners();
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: Container(
                color: Colors.white,
                height: 100,
                child: const Center(
                  child: Text('Let`s continue!'),
                ),
              ),
            ));
  }

  Future<void> leftRoom() async {
    socketService!.emit(SocketEvent.leftSingleRoom, {
      'data': {'roomId': null}
    });
  }

  Future<void> joinRoom() async {
    socketService!.emit(SocketEvent.joinSingleRoom, {
      'data': {'roomId': null}
    });
  }

  void showOption() {
    isShowOption = !isShowOption;
    notifyListeners();
  }

  @override
  void onAppear() {
    // TODO: implement onAppear
  }

  @override
  void onDisAppear() {
    // TODO: implement onDisAppear
  }

  void getGoals() async {
    try {
      var res = await goalService.getAllGoals();
      goals = res.data;
    } on DioError catch (e) {
      debugPrint(e.message);
    }
    notifyListeners();
  }
}
