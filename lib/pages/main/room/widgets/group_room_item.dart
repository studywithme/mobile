import 'package:flutter/material.dart';
import 'package:studywithme/models/room.dart';
import 'package:studywithme/pages/main/room/lobby_page/lobby_page.dart';

class GroupRoomItem extends StatelessWidget {
  final Room room;
  final Function(Room) onActionFavourite;
  const GroupRoomItem(
      {Key? key, required this.room, required this.onActionFavourite})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Card(
        elevation: 3,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
        child: Container(
          height: 180,
          decoration: BoxDecoration(
              image: const DecorationImage(
                  image: NetworkImage(
                      'https://upload.wikimedia.org/wikipedia/commons/3/36/Hopetoun_falls.jpg'),
                  fit: BoxFit.cover),
              borderRadius: BorderRadius.circular(20)),
          child: Column(
            children: [
              Container(
                height: 80,
                padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 4),
                color: Colors.transparent,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: (room.currentUsers != null &&
                                      room.currentUsers! > 0)
                                  ? Colors.greenAccent
                                  : Colors.yellowAccent),
                          child: Text(
                              '${(room.currentUsers != null && room.currentUsers! > 0) ? 'Live' : 'Inactive'}'),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Text(
                          room.name,
                          style: const TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                    const Spacer(),
                    Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 10),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20)),
                      child: Row(
                        children: [
                          const Icon(
                            Icons.group_outlined,
                            color: Colors.white,
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          Text('${room.currentUsers} / ${room.maxCount}',
                              style: const TextStyle(color: Colors.white))
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                height: 100,
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 2),
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20)),
                    color: Colors.white),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      room.description ?? 'Study with me',
                      style: TextStyle(color: Colors.black),
                      maxLines: 4,
                    ),
                    const Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        InkWell(
                          onTap: () {
                            onActionFavourite.call(room);
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border:
                                    Border.all(color: Colors.deepPurpleAccent)),
                            child: Center(
                              child: room.isFavourite
                                  ? const Icon(
                                      Icons.favorite,
                                      color: Colors.pink,
                                    )
                                  : const Icon(
                                      Icons.favorite_outline,
                                      color: Colors.deepPurpleAccent,
                                    ),
                            ),
                          ),
                        ),
                        // Container(
                        //   padding: const EdgeInsets.symmetric(
                        //       vertical: 5, horizontal: 10),
                        //   decoration: BoxDecoration(
                        //       borderRadius: BorderRadius.circular(10),
                        //       border:
                        //           Border.all(color: Colors.deepPurpleAccent)),
                        //   child: const Center(
                        //     child: Icon(
                        //       Icons.link,
                        //       color: Colors.deepPurpleAccent,
                        //     ),
                        //   ),
                        // ),
                        InkWell(
                          onTap: () {
                            if ((room.isAuthor == null ||
                                    room.isAuthor == false) &&
                                (room.currentUsers == null ||
                                    room.currentUsers == 0)) {
                              showDialog(
                                  context: context,
                                  builder: (context) => Dialog(
                                        child: Container(
                                            height: 100,
                                            color: Colors.white,
                                            child: const Center(
                                              child: Text(
                                                'Room is not working now!',
                                                style: TextStyle(fontSize: 20),
                                              ),
                                            )),
                                      ));
                            } else {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => LobbyPage(
                                            room: room,
                                          )));
                            }
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            decoration: BoxDecoration(
                              color: Colors.deepOrangeAccent,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Row(
                              children: const [
                                Icon(
                                  Icons.navigate_next,
                                  color: Colors.white,
                                ),
                                Text(
                                  'Join Group',
                                  style: TextStyle(color: Colors.white),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
