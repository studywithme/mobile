import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:studywithme/models/goal.dart';

import '../../../../base/base_viewmodel.dart';
import '../../../../base/di/locator.dart';
import '../../../../services/remote/api_client.dart';

class SessionGoalVM extends BaseViewModel {
  final goalService = locator<ApiClient>().goalService;
  final TextEditingController goalController = TextEditingController();
  final FocusNode focusNode = FocusNode();
  List<Goal> goals = [];
  List<Goal> openGoals = [];
  List<Goal> completedGoals = [];
  int completed = 0;
  int open = 0;
  bool isOpenPage = true;

  @override
  void onInit() {}

  void initGoal(goals) {
    this.goals = goals;
    getOpenCompleted();
    notifyListeners();
  }

  void createGoal() async {
    if (goalController.text.isNotEmpty) {
      try {
        Goal goal =
            await goalService.createNewGoal({'title': goalController.text});
        goals.add(goal);
        getOpenCompleted();
      } on DioError catch (e) {
        debugPrint("Error goal Service: ${e.message}");
      } finally {
        goalController.clear();
      }
    }
    focusNode.unfocus();
    notifyListeners();
  }

  void updateStatus(Goal oldGoal) {
    // oldGoal.status = oldGoal.status == 'open' ? 'completed' : 'open';
    // notifyListeners();
    notifyListeners();
    updateGoal(oldGoal);
  }

  void updateGoal(Goal goal) async {
    try {
      final res = await goalService.updateGoal(goal.toJson());
      if (res.result) {
        for (var element in goals) {
          if (element.id == goal.id) {
            element = goal;
          }
        }
        getOpenCompleted();
        notifyListeners();
      }
    } on DioError catch (e) {
      debugPrint("Error update goal ${e.message}");
    }
  }

  void changeStatus(int id) async {
    var goal = goals.firstWhere((element) => element.id == id);
    goal.status = goal.status == 'open' ? 'completed' : 'open';
    await goalService.updateGoal(goal.toJson());
    notifyListeners();
  }

  void deleteGoal(int id) async {
    try {
      final res = await goalService.delete(id);
      if (res.result) {
        goals.removeWhere((element) => element.id == id);
        getOpenCompleted();
        notifyListeners();
      }
    } on DioError catch (e) {
      debugPrint("Error delete goal ${e.message}");
    }
  }

  void getOpenCompleted() {
    completed = 0;
    open = 0;
    openGoals = [];
    completedGoals = [];
    for (var element in goals) {
      if (element.status == 'completed') {
        completed++;
        completedGoals.add(element);
      } else {
        open++;
        debugPrint('Data: ${jsonEncode(element.toJson())}');
        openGoals.add(element);
      }
    }
    notifyListeners();
  }

  void onChange(String value) {
    goalController.text = value;
    notifyListeners();
  }

  void changePage(bool isOpen) {
    isOpenPage = isOpen;
    notifyListeners();
  }

  @override
  void onAppear() {}

  @override
  void onDisAppear() {}
}
