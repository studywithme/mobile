import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:studywithme/components/badge.dart';
import 'package:studywithme/models/badge_type.dart';
import 'package:studywithme/models/room.dart';
import 'package:studywithme/pages/main/room/solo_room/solo_room.dart';
import '../../../../utils/size_common.dart';

class SoloRoomItem extends StatelessWidget {
  final Room roomInfo;
  const SoloRoomItem({Key? key, required this.roomInfo}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Container(
        height: 80,
        padding: const EdgeInsets.all(5),
        decoration: BoxDecoration(
            // image: const DecorationImage(
            //     image: NetworkImage(
            //         "https://upload.wikimedia.org/wikipedia/commons/3/36/Hopetoun_falls.jpg"),
            //     fit: BoxFit.cover),
            borderRadius: BorderRadius.circular(20)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  padding:
                      const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.greenAccent),
                  child: const Text('Live'),
                ),
                const SizedBox(
                  width: 10,
                ),
                // const Text('Solo Study Room'),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            const Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const BadgeWidget(image: '', type: BadgeType.gold),
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const SoloRoom()));
                  },
                  child: Container(
                    padding:
                        const EdgeInsets.symmetric(vertical: 2, horizontal: 5),
                    decoration: BoxDecoration(
                      color: Colors.deepOrangeAccent,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Row(
                      children: const [
                        Icon(
                          Icons.navigate_next,
                          color: Colors.white,
                        ),
                        Text(
                          'Start solo study',
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
