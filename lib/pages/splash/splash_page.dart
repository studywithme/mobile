import 'dart:async';

import 'package:flutter/material.dart';
import 'package:studywithme/gen/assets.gen.dart';
import 'package:studywithme/pages/splash/splash_vm.dart';
import 'package:after_layout/after_layout.dart';

import '../../base/base_page.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage>
    with MixinBasePage<SplashVM>, AfterLayoutMixin<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return builder(() => Scaffold(
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: Assets.icons.bgSplash.svg(),
                ),
                const Padding(padding: EdgeInsets.only(top: 20.0)),
                const Text(
                  "Study With Me",
                  style: TextStyle(fontSize: 20.0, color: Colors.blueAccent),
                ),
                const Padding(padding: EdgeInsets.only(top: 20.0)),
                const CircularProgressIndicator(
                  backgroundColor: Colors.blueAccent,
                  strokeWidth: 1,
                )
              ],
            ),
          ),
        ));
  }

  @override
  FutureOr<void> afterFirstLayout(BuildContext context) {
    provider.checkFirstOpenApp(context, mounted);
  }

  @override
  SplashVM create() => SplashVM();

  @override
  void initialise(BuildContext context) {
    // TODO: implement initialise
  }
}
