import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:studywithme/base/di/locator.dart';
import 'package:studywithme/constants/app_constants.dart';
import 'package:studywithme/pages/auth/login/login_page.dart';
import 'package:studywithme/services/remote/api_client.dart';

import '../../base/base_viewmodel.dart';
import '../../services/local/shared_prefs.dart';
import '../main/app_page.dart';

class SplashVM extends BaseViewModel {
  @override
  void onInit() {}

  Future<void> checkFirstOpenApp(BuildContext context, mounted) async {
    await Future.delayed(const Duration(seconds: 2));
    // [TODO] Check have login
    // Get token from sharedPref
    var error = false;
    try {
      final data = await locator<ApiClient>().roomService.getRooms();
    } on DioError catch (e) {
      error = true;
    }
    if (!mounted) return;
    if (error) {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => LoginPage()));
    } else {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => const AppPage()));
    }
  }

  @override
  void onAppear() {
    // TODO: implement onAppear
  }

  @override
  void onDisAppear() {
    // TODO: implement onDisAppear
  }
}
