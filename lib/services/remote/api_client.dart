import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:retrofit/retrofit.dart';
import 'package:studywithme/constants/app_constants.dart';
import 'package:studywithme/services/local/shared_prefs.dart';
import 'package:studywithme/services/remote/auth_service.dart';
import 'package:studywithme/services/remote/board_service.dart';
import 'package:studywithme/services/remote/chat_service.dart';
import 'package:studywithme/services/remote/favourite_service.dart';
import 'package:studywithme/services/remote/goal_service.dart';
import 'package:studywithme/services/remote/room_service.dart';
import 'package:studywithme/services/remote/user_service.dart';

import '../../components/error_popup.dart';
import '../../generated/l10n.dart';
import '../../main.dart';

class ApiClient {
  final dio = Dio();
  late AuthService authService;
  late RoomService roomService;
  late ChatService chatService;
  late GoalService goalService;
  late BoardService boardService;
  late UserService userService;
  late FavouriteService favouriteService;
  ApiClient() {
    dio.options.connectTimeout = 50000;
    dio.options.receiveTimeout = 5000;
    dio.options.contentType = 'application/json';
    // debugger
    if (kDebugMode) {
      dio.interceptors.add(PrettyDioLogger(
          request: false,
          requestHeader: true,
          requestBody: true,
          responseBody: false,
          responseHeader: false));
    }
    // add handle request, response, error when call api
    dio.interceptors.add(InterceptorsWrapper(onRequest: (option, handle) async {
      final token = SharedPrefs.getValue(AppConstant.accessToken);
      debugPrint("TOKEN: $token");
      if (token.isNotEmpty) {
        option.headers['authorization'] = 'Bearer $token';
      } else {
        option.headers['authorization'] = 'Bearer';
      }
      return handle.next(option);
    }, onResponse: (res, handler) async {
      return handler.next(res);
    }, onError: (error, handler) async {
      final checkConnect = await (Connectivity().checkConnectivity());
      if (checkConnect == ConnectivityResult.none) {
        error.error = 'No internet connection. Please check your connection';
        ErrorPopup.show(
            MyApp.context!, S.of(MyApp.context!).message_error_lost_connect);
      }
      return handler.next(error);
    }));

    // instance Rest Client
    authService = AuthService(dio);
    roomService = RoomService(dio);
    chatService = ChatService(dio);
    goalService = GoalService(dio);
    boardService = BoardService(dio);
    userService = UserService(dio);
    favouriteService = FavouriteService(dio);
  }
}
