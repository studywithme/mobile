import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:studywithme/services/remote/responses/users_response.dart';

import 'bodies/login_body.dart';
import 'responses/profile_response.dart';
part 'auth_service.g.dart';

@RestApi(baseUrl: 'https://studywithmeserver12.herokuapp.com/api/')
abstract class AuthService {
  factory AuthService(Dio dio, {String baseUrl}) = _AuthService;
  @POST('auth/login')
  Future<ProfileResponse> login(@Body() LoginBody data);

  @POST('auth/register')
  Future<ProfileResponse> register(@Body() dynamic data);

  @GET('user/all')
  Future<UsersResponse> getAllUsers();
}
