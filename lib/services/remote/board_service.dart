import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:studywithme/services/remote/responses/channel_chat_response.dart';
import 'package:studywithme/services/remote/responses/rank_response.dart';
part 'board_service.g.dart';

@RestApi(baseUrl: 'https://studywithmeserver12.herokuapp.com/api/board/')
abstract class BoardService {
  factory BoardService(Dio dio) = _BoardService;

  @GET('leader')
  Future<RankResponse> getBoard();
}
