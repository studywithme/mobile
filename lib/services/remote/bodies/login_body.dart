import 'package:json_annotation/json_annotation.dart';
part 'login_body.g.dart';

@JsonSerializable()
class LoginBody {
  @JsonKey(name: 'email')
  final String email;

  @JsonKey(name: 'password')
  final String password;

  LoginBody({required this.email, required this.password});

  factory LoginBody.fromJson(Map<String, dynamic> json) =>
      _$LoginBodyFromJson(json);

  Map<String, dynamic> toJson() => _$LoginBodyToJson(this);
}
