import 'package:json_annotation/json_annotation.dart';
import 'package:studywithme/models/room.dart';
part 'room_body.g.dart';

@JsonSerializable()
class RoomBody {
  late String name;
  late RoomType type;
  late int limit = 15;
  String? description;

  RoomBody();

  factory RoomBody.fromJson(Map<String, dynamic> json) =>
      _$RoomBodyFromJson(json);

  Map<String, dynamic> toJson() => _$RoomBodyToJson(this);
}
