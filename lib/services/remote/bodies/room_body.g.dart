// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomBody _$RoomBodyFromJson(Map<String, dynamic> json) => RoomBody()
  ..name = json['name'] as String
  ..type = $enumDecode(_$RoomTypeEnumMap, json['type'])
  ..limit = json['limit'] as int
  ..description = json['description'] as String?;

Map<String, dynamic> _$RoomBodyToJson(RoomBody instance) => <String, dynamic>{
      'name': instance.name,
      'type': _$RoomTypeEnumMap[instance.type]!,
      'limit': instance.limit,
      'description': instance.description,
    };

const _$RoomTypeEnumMap = {
  RoomType.solo: 'solo',
  RoomType.group: 'group',
};
