import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:studywithme/services/remote/responses/channel_chat_response.dart';
import 'package:studywithme/services/remote/responses/channel_chats_response.dart';
part 'chat_service.g.dart';

@RestApi(baseUrl: 'https://studywithmeserver12.herokuapp.com/api/chat/')
abstract class ChatService {
  factory ChatService(Dio dio) = _ChatService;

  @GET('channel/{channelId}')
  Future<ChannelChatResponse> getChannelChatById(
      @Path('channelId') String channelId);

  @GET('channel/{channelName}')
  Future<ChannelChatResponse> getChannelChatByName(
      @Path('channelName') String channelName);

  @GET('allChannel/{channelType}')
  Future<ChannelChatsResponse> getChannelChat(
      @Path('channelType') String channelType);
}
