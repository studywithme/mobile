import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:studywithme/services/remote/responses/update_response.dart';
part 'favourite_service.g.dart';

@RestApi(baseUrl: 'https://studywithmeserver12.herokuapp.com/api/favourite/')
abstract class FavouriteService {
  factory FavouriteService(Dio dio) = _FavouriteService;

  @PUT('add')
  Future<UpdateResponse> addFavourite(@Body() dynamic data);

  @PUT('remove')
  Future<UpdateResponse> removeFavourite(@Body() dynamic data);
}
