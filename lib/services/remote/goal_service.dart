import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:studywithme/models/goal.dart';
import 'package:studywithme/services/remote/responses/update_response.dart';
import 'package:studywithme/services/remote/responses/goals_response.dart';
part 'goal_service.g.dart';

@RestApi(baseUrl: 'https://studywithmeserver12.herokuapp.com/api/goals/')
abstract class GoalService {
  factory GoalService(Dio dio) = _GoalService;

  @GET('all')
  Future<GoalsResponse> getAllGoals();

  @POST('add')
  Future<Goal> createNewGoal(@Body() dynamic data);

  @PUT('update')
  Future<UpdateResponse> updateGoal(@Body() dynamic data);

  @DELETE('{goalId}')
  Future<UpdateResponse> delete(@Path('goalId') int id);
}
