import 'package:json_annotation/json_annotation.dart';
part 'avatar_response.g.dart';

@JsonSerializable()
class AvatarResponse {
  String? path;

  AvatarResponse();

  factory AvatarResponse.fromJson(Map<String, dynamic> json) =>
      _$AvatarResponseFromJson(json);
}
