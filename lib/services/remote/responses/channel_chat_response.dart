import 'package:json_annotation/json_annotation.dart';
import 'package:studywithme/models/chat_channel.dart';
part 'channel_chat_response.g.dart';

@JsonSerializable()
class ChannelChatResponse {
  late ChatChannel data;

  ChannelChatResponse();

  factory ChannelChatResponse.fromJson(Map<String, dynamic> json) =>
      _$ChannelChatResponseFromJson(json);
}
