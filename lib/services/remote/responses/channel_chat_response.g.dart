// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'channel_chat_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChannelChatResponse _$ChannelChatResponseFromJson(Map<String, dynamic> json) =>
    ChannelChatResponse()
      ..data = ChatChannel.fromJson(json['data'] as Map<String, dynamic>);

Map<String, dynamic> _$ChannelChatResponseToJson(
        ChannelChatResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
