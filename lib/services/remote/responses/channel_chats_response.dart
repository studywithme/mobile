import 'package:json_annotation/json_annotation.dart';
import 'package:studywithme/models/chat_channel.dart';
part 'channel_chats_response.g.dart';

@JsonSerializable()
class ChannelChatsResponse {
  late List<ChatChannel> data;

  ChannelChatsResponse();

  factory ChannelChatsResponse.fromJson(Map<String, dynamic> json) =>
      _$ChannelChatsResponseFromJson(json);
}
