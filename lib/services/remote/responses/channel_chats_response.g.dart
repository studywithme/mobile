// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'channel_chats_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChannelChatsResponse _$ChannelChatsResponseFromJson(
        Map<String, dynamic> json) =>
    ChannelChatsResponse()
      ..data = (json['data'] as List<dynamic>)
          .map((e) => ChatChannel.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$ChannelChatsResponseToJson(
        ChannelChatsResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
