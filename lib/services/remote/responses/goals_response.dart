import 'package:json_annotation/json_annotation.dart';
import 'package:studywithme/models/goal.dart';
part 'goals_response.g.dart';

@JsonSerializable()
class GoalsResponse {
  late List<Goal> data;

  GoalsResponse();

  factory GoalsResponse.fromJson(Map<String, dynamic> json) =>
      _$GoalsResponseFromJson(json);
}
