import 'package:json_annotation/json_annotation.dart';

import '../../../models/user_model.dart';
part 'profile_response.g.dart';

@JsonSerializable()
class ProfileResponse {
  @JsonKey(name: 'user')
  late UserModel data;

  @JsonKey(name: 'token')
  String? token;

  ProfileResponse();

  factory ProfileResponse.fromJson(Map<String, dynamic> json) =>
      _$ProfileResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileResponseToJson(this);
}
