import 'package:json_annotation/json_annotation.dart';
import 'package:studywithme/models/all_rank.dart';
import 'package:studywithme/models/daily.dart';

import '../../../models/monthly.dart';

part 'rank_response.g.dart';

@JsonSerializable()
class RankResponse {
  late Daily daily;
  late Monthly monthly;
  late AllRank all;

  RankResponse();

  factory RankResponse.fromJson(Map<String, dynamic> json) =>
      _$RankResponseFromJson(json);
}
