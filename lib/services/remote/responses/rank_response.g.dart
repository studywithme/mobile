// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rank_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RankResponse _$RankResponseFromJson(Map<String, dynamic> json) => RankResponse()
  ..daily = Daily.fromJson(json['daily'] as Map<String, dynamic>)
  ..monthly = Monthly.fromJson(json['monthly'] as Map<String, dynamic>)
  ..all = AllRank.fromJson(json['all'] as Map<String, dynamic>);

Map<String, dynamic> _$RankResponseToJson(RankResponse instance) =>
    <String, dynamic>{
      'daily': instance.daily,
      'monthly': instance.monthly,
      'all': instance.all,
    };
