import 'package:json_annotation/json_annotation.dart';
import 'package:studywithme/models/room.dart';
part 'rooms_response.g.dart';

@JsonSerializable()
class RoomsResponse {
  @JsonKey(name: 'data')
  late List<Room> data;

  RoomsResponse();

  factory RoomsResponse.fromJson(Map<String, dynamic> json) =>
      _$RoomsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$RoomsResponseToJson(this);
}
