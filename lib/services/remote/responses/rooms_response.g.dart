// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rooms_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomsResponse _$RoomsResponseFromJson(Map<String, dynamic> json) =>
    RoomsResponse()
      ..data = (json['data'] as List<dynamic>)
          .map((e) => Room.fromJson(e as Map<String, dynamic>))
          .toList();

Map<String, dynamic> _$RoomsResponseToJson(RoomsResponse instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
