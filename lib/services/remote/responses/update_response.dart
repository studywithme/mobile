import 'package:json_annotation/json_annotation.dart';
import 'package:studywithme/models/goal.dart';
part 'update_response.g.dart';

@JsonSerializable()
class UpdateResponse {
  late bool result;

  UpdateResponse();

  factory UpdateResponse.fromJson(Map<String, dynamic> json) =>
      _$UpdateResponseFromJson(json);
}
