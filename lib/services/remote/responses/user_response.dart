import 'package:json_annotation/json_annotation.dart';
import 'package:studywithme/models/user_model.dart';
part 'user_response.g.dart';

@JsonSerializable()
class UserResponse {
  late UserModel data;

  UserResponse();

  factory UserResponse.fromJson(Map<String, dynamic> json) =>
      _$UserResponseFromJson(json);
}
