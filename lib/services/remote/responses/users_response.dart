import 'package:json_annotation/json_annotation.dart';
import 'package:studywithme/models/user_model.dart';
part 'users_response.g.dart';

@JsonSerializable()
class UsersResponse {
  late List<UserModel> data;

  UsersResponse();

  factory UsersResponse.fromJson(Map<String, dynamic> json) =>
      _$UsersResponseFromJson(json);
}
