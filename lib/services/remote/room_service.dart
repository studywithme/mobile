import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:studywithme/models/room.dart';
import 'package:studywithme/services/remote/bodies/room_body.dart';
import 'package:studywithme/services/remote/responses/rooms_response.dart';
import 'package:studywithme/services/remote/responses/update_response.dart';

part 'room_service.g.dart';

@RestApi(baseUrl: 'https://studywithmeserver12.herokuapp.com/api/')
abstract class RoomService {
  factory RoomService(Dio dio, {String baseUrl}) = _RoomService;

  @GET('room/all')
  Future<RoomsResponse> getRooms();

  @POST('room/create')
  Future<Room> createNewRoom(@Body() RoomBody body);

  @GET('room/{id}/token')
  Future<Map<String, String>> getRoomRtcToken(@Path('id') int id);

  @GET('favourite/all')
  Future<RoomsResponse> getFavouriteRooms();

  @PUT('favourite/add')
  Future<UpdateResponse> addFavourite();

  @PUT('favourite/remove')
  Future<UpdateResponse> removeFavourite();

  @GET('room/my-room')
  Future<RoomsResponse> getMyRooms();
}
