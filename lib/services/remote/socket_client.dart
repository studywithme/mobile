import 'dart:async';
import 'package:flutter/material.dart';
import 'package:socket_io_client/socket_io_client.dart' as io;
import 'package:studywithme/constants/socket_event.dart';

class StreamSocket {
  io.Socket? socket;

  final _socketResponse = StreamController<Map<String, dynamic>>.broadcast();

  void Function(Map<String, dynamic>) get addResponse =>
      _socketResponse.sink.add;

  Stream<Map<String, dynamic>> get getResponse => _socketResponse.stream;

  void dispose() {
    _socketResponse.close();
  }

  void setSocket(socket) {
    this.socket = socket;
  }

  void emitEvent(String event, Map<String, dynamic> data) {
    final socket = this.socket;
    if (socket != null) {
      debugPrint("Emit event $event");
      socket.emit(event, data);
    }
  }

  void setupListener(initSocket) {
    socket = initSocket;
    if (socket != null) {
      socket?.onConnect((_) {
        debugPrint('Connect socket success');
        // emitEvent(event, data)
      });
      socket?.on(SocketEvent.userJoined, (data) {
        _socketResponse.sink.add({'USER_JOINED': data});
      });
      socket?.on(SocketEvent.userLeft, (data) {
        _socketResponse.sink.add({'USER_LEFT': data});
      });
    }
  }
}
