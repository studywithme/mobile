import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:studywithme/services/remote/responses/avatar_response.dart';
import 'package:studywithme/services/remote/responses/user_response.dart';
part 'user_service.g.dart';

@RestApi(baseUrl: 'https://studywithmeserver12.herokuapp.com/api/user/')
abstract class UserService {
  factory UserService(Dio dio, {String baseUrl}) = _UserService;

  @GET('profile')
  Future<UserResponse> getProfile();

  @POST('update')
  Future<UserResponse> updateProfile(@Body() dynamic data);

  @POST('update-avatar')
  @MultiPart()
  Future<AvatarResponse> updateAvatar(@Body() FormData data);
}
