import 'package:intl/intl.dart';

extension DateTimeToString on DateTime {
  dateTimeToString() {
    return '$year-${month < 10 ? '0$month' : month}-$day';
  }
}

extension StringX on String {
  String get dateTime {
    DateFormat dateTimeFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
    DateTime dateTime = dateTimeFormat.parse(this);
    DateFormat timeFormat = DateFormat("HH:mm");
    String time = timeFormat.format(dateTime);
    return time;
  }

  String get username {
    return split("@")[0];
  }
}
